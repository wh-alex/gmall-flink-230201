package com.atguigu.gmallpublisher.controller;

import com.atguigu.gmallpublisher.service.SugarService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

//@Controller
@RestController
public class SugarController {

    @Autowired
    private SugarService sugarService;

    @RequestMapping("/test")
    //@ResponseBody
    public String test01() {
        return "success";
    }

    @RequestMapping("/test02")
    public String test02(@RequestParam("name") String nn,
                         @RequestParam(value = "age", defaultValue = "18") int age) {
        System.out.println(nn + ":" + age);
        return "aaaaaaa";
    }

    @RequestMapping("/gmv")
    public String getGmv(@RequestParam(value = "date", defaultValue = "0") String date) {

        if ("0".equals(date)) {
            date = getToday();
        }

        BigDecimal gmv = sugarService.getGmv(date);
        if (gmv == null) {
            gmv = new BigDecimal("0.0");
        }

        return "{\n" +
                "  \"status\": 0,\n" +
                "  \"msg\": \"\",\n" +
                "  \"data\": " + gmv + "\n" +
                "}";
    }

    @RequestMapping("/trademark")
    public String getGmvByTrademark(@RequestParam(value = "date", defaultValue = "0") String date,
                                    @RequestParam(value = "limit", defaultValue = "5") int limit) {

        if ("0".equals(date)) {
            date = getToday();
        }

        //查询结果
        Map<String, BigDecimal> gmvByTrademarkName = sugarService.getGmvByTrademarkName(date, limit);

        Set<String> trademarkName = gmvByTrademarkName.keySet();
        Collection<BigDecimal> gmvs = gmvByTrademarkName.values();

        return "{\n" +
                "  \"status\": 0,\n" +
                "  \"msg\": \"\",\n" +
                "  \"data\": {\n" +
                "    \"categories\": [\"" + StringUtils.join(trademarkName, "\",\"") + "\"],\n" +
                "    \"series\": [\n" +
                "      {\n" +
                "        \"name\": \"品牌GMV\",\n" +
                "        \"data\": [" + StringUtils.join(gmvs, ",") + "]\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";

    }

    private String getToday() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(System.currentTimeMillis());
    }

}
