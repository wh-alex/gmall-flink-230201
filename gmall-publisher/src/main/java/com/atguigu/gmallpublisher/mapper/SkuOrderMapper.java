package com.atguigu.gmallpublisher.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface SkuOrderMapper {

    @Select("SELECT sum(order_amount) FROM dws_trade_sku_order_window WHERE cur_date='${date}'")
    BigDecimal selectGmv(@Param("date") String date);

    @Select("SELECT trademark_name,sum(order_amount) order_amount,count(*) ct FROM dws_trade_sku_order_window WHERE cur_date='${date}' GROUP BY trademark_name ORDER BY order_amount DESC LIMIT ${limit}")
    List<Map> selectGmvByTrademark(@Param("date") String date,
                                   @Param("limit") int limit);

}
