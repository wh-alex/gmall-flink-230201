package com.atguigu.gmallpublisher.service.impl;

import com.atguigu.gmallpublisher.mapper.SkuOrderMapper;
import com.atguigu.gmallpublisher.service.SugarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SugarServiceImpl implements SugarService {

    @Autowired
    private SkuOrderMapper skuOrderMapper;

    @Override
    public BigDecimal getGmv(String date) {
        return skuOrderMapper.selectGmv(date);
    }

    @Override
    public Map<String, BigDecimal> getGmvByTrademarkName(String date, int limit) {

        //查询结果
        List<Map> gmvByTrademark = skuOrderMapper.selectGmvByTrademark(date, limit);

        //创建HashMap存放结果数据
        HashMap<String, BigDecimal> resultMap = new HashMap<>();

        //遍历gmvByTrademark集合,将数据取出放入resultMap
        for (Map map : gmvByTrademark) {
            resultMap.put((String) map.get("trademark_name"), (BigDecimal) map.get("order_amount"));
        }

        //返回结果
        return resultMap;
    }
}
