package com.atguigu.app;

import com.atguigu.bean.WaterSensor1;
import com.atguigu.bean.WaterSensor2;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class Test02_LeftJoin {

    public static void main(String[] args) {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        SingleOutputStreamOperator<WaterSensor1> ds1 = env.socketTextStream("hadoop102", 8888)
                .map(line -> {
                    String[] split = line.split(",");
                    return new WaterSensor1(split[0], Long.parseLong(split[1]), Double.parseDouble(split[2]));
                });
        tableEnv.createTemporaryView("t1", ds1);

        SingleOutputStreamOperator<WaterSensor2> ds2 = env.socketTextStream("hadoop102", 9999)
                .map(line -> {
                    String[] split = line.split(",");
                    return new WaterSensor2(split[0], split[1], Double.parseDouble(split[2]));
                });
        tableEnv.createTemporaryView("t2", ds2);

        tableEnv.executeSql("" +
                "create table test(\n" +
                "    id string,\n" +
                "    ts bigint,\n" +
                "    name string,\n" +
                "    vc1 double,\n" +
                "    vc2 double,\n" +
                "    cur_ts TIMESTAMP_LTZ(3)," +
                "    primary key(id) NOT ENFORCED\n" +
                ")" + KafkaUtil.getUpsertKafkaSinkDDL("test"));

        tableEnv.executeSql("insert into test " +
                "select t1.id,t1.ts,t2.name,t1.vc vc1,t2.vc vc2,CURRENT_ROW_TIMESTAMP() cur_ts from t1 left join t2 on t1.id=t2.id");

    }

}
