package com.atguigu.app;

import com.alibaba.fastjson.JSON;
import com.atguigu.bean.Site;
import org.apache.doris.flink.cfg.DorisExecutionOptions;
import org.apache.doris.flink.cfg.DorisOptions;
import org.apache.doris.flink.cfg.DorisReadOptions;
import org.apache.doris.flink.sink.DorisSink;
import org.apache.doris.flink.sink.writer.SimpleStringSerializer;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Properties;

public class Test_Doris_Write_Json {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<Site> elementsDS = env.fromElements(new Site((short) 3, 1005, "zhangsan", 2L),
                new Site((short) 2, 1006, "lisi", 5L));

        DorisOptions dorisOptions = DorisOptions.builder()
                .setFenodes("hadoop102:7030")
                .setTableIdentifier("test.table1")
                .setUsername("root")
                .setPassword("000000")
                .build();

        Properties properties = new Properties();
        properties.setProperty("format", "json");
        properties.setProperty("read_json_by_line", "true"); // 每行一条 json 数据

        DorisSink<String> dorisSink = DorisSink.<String>builder()
                .setDorisOptions(dorisOptions)
                .setDorisReadOptions(DorisReadOptions.builder().build())
                .setSerializer(new SimpleStringSerializer())
                .setDorisExecutionOptions(DorisExecutionOptions
                        .builder()
                        .disable2PC()
                        .setLabelPrefix("")
                        .setDeletable(false)
                        .setBufferCount(5)
                        .setBufferSize(1024 * 1024)
                        .setCheckInterval(5)
                        .setMaxRetries(3)
                        .setStreamLoadProp(properties)
                        .build())
                .build();

        elementsDS
                .map(JSON::toJSONString)
                .sinkTo(dorisSink);

        env.execute();
    }
}