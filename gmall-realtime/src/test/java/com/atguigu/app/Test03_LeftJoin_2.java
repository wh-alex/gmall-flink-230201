package com.atguigu.app;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.RichFilterFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Test03_LeftJoin_2 {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<String> kafkaDS = env.fromSource(KafkaUtil.getKafkaSource("test", "test_230201"), WatermarkStrategy.noWatermarks(), "kafka-source");

        //过滤Null值
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                if (value != null) {
                    out.collect(JSON.parseObject(value));
                }
            }
        });

        //方案二：去重  任取一条即可,一般取第一条
        jsonObjDS.keyBy(jsonObj -> jsonObj.getString("id"))
                .filter(new RichFilterFunction<JSONObject>() {

                    private ValueState<String> existState;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        existState = getRuntimeContext().getState(new ValueStateDescriptor<String>("state", String.class));
                    }

                    @Override
                    public boolean filter(JSONObject value) throws Exception {

                        String state = existState.value();
                        if (state == null) {
                            existState.update("1");
                            return true;
                        } else {
                            return false;
                        }
                    }
                })
                .print(">>>");

        env.execute();

    }

}
