package com.atguigu.app;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Test04_LeftJoin_3 {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DataStreamSource<String> kafkaDS = env.fromSource(KafkaUtil.getKafkaSource("test", "test_230201"), WatermarkStrategy.noWatermarks(), "kafka-source");

        //过滤Null值
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                if (value != null) {
                    out.collect(JSON.parseObject(value));
                }
            }
        });

        //方案三：去重  累加型需求  输出过的数据输出0
        jsonObjDS.keyBy(jsonObj -> jsonObj.getString("id"))
                .map(new RichMapFunction<JSONObject, JSONObject>() {

                    private ValueState<String> valueState;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        valueState = getRuntimeContext().getState(new ValueStateDescriptor<String>("state", String.class));
                    }

                    @Override
                    public JSONObject map(JSONObject value) throws Exception {

                        String state = valueState.value();

                        //左表
                        if (state != null) {
                            value.put("vc1", 0.0D);
                        }
                        valueState.update("1");

                        //右表
                        Double vc2 = value.getDouble("vc2");
                        if (vc2 == null) {
                            value.put("vc2", 0.0D);
                        }

                        return value;
                    }
                })
                .print(">>>");

        env.execute();

    }

}
