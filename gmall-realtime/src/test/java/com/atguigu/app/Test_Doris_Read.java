package com.atguigu.app;

import com.atguigu.bean.Site;
import org.apache.doris.flink.cfg.DorisOptions;
import org.apache.doris.flink.cfg.DorisReadOptions;
import org.apache.doris.flink.deserialization.SimpleListDeserializationSchema;
import org.apache.doris.flink.source.DorisSource;
import org.apache.doris.flink.source.DorisSourceBuilder;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.List;

public class Test_Doris_Read {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        DorisOptions.Builder builder = DorisOptions.builder()
                .setFenodes("hadoop102:7030")
                .setTableIdentifier("test.table1")
                .setUsername("root")
                .setPassword("000000");

        DorisSource<List<?>> dorisSource = DorisSourceBuilder.<List<?>>builder()
                .setDorisOptions(builder.build())
                .setDorisReadOptions(DorisReadOptions.builder().build())
                .setDeserializer(new SimpleListDeserializationSchema())
                .build();

        DataStreamSource<List<?>> dorisDS = env.fromSource(dorisSource, WatermarkStrategy.noWatermarks(), "doris source");

        dorisDS.map(new MapFunction<List<?>, Site>() {
            @Override
            public Site map(List<?> value) throws Exception {
                return new Site(
                        Short.parseShort(value.get(1).toString()),
                        Integer.parseInt(value.get(0).toString()),
                        value.get(2).toString(),
                        Long.parseLong(value.get(3).toString()));
            }
        }).print();

        env.execute();

    }

}
