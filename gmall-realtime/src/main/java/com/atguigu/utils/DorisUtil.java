package com.atguigu.utils;

import org.apache.doris.flink.cfg.DorisExecutionOptions;
import org.apache.doris.flink.cfg.DorisOptions;
import org.apache.doris.flink.cfg.DorisReadOptions;
import org.apache.doris.flink.sink.DorisSink;
import org.apache.doris.flink.sink.writer.SimpleStringSerializer;

import java.util.Properties;

public class DorisUtil {

    public static DorisSink<String> getDorisSink(String databaseAndTable) {
        DorisOptions dorisOptions = DorisOptions.builder()
                .setFenodes("hadoop102:7030")
                .setTableIdentifier(databaseAndTable)
                .setUsername("root")
                .setPassword("000000")
                .build();

        Properties properties = new Properties();
        properties.setProperty("format", "json");
        properties.setProperty("read_json_by_line", "true"); // 每行一条 json 数据

        return DorisSink.<String>builder()
                .setDorisOptions(dorisOptions)
                .setDorisReadOptions(DorisReadOptions.builder().build())
                .setSerializer(new SimpleStringSerializer())
                .setDorisExecutionOptions(DorisExecutionOptions
                        .builder()
                        .disable2PC()
                        .setLabelPrefix("")
                        .setDeletable(false)
                        .setBufferCount(5)
                        .setBufferSize(1024 * 1024)
                        .setCheckInterval(5)
                        .setMaxRetries(3)
                        .setStreamLoadProp(properties)
                        .build())
                .build();
    }

}
