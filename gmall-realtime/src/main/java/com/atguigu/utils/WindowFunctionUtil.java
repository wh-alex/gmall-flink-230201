package com.atguigu.utils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.lang.reflect.InvocationTargetException;

public class WindowFunctionUtil {

    public static <T> void setSttAndEdt(TimeWindow window, Iterable<T> input, Collector<T> out) throws InvocationTargetException, IllegalAccessException {

        //取值
        T next = input.iterator().next();

        //给stt edt设置值
        BeanUtils.setProperty(next, "stt", DateFormatUtil.toYmdHms(window.getStart()));
        BeanUtils.setProperty(next, "edt", DateFormatUtil.toYmdHms(window.getEnd()));

        //输出数据
        out.collect(next);
    }
}
