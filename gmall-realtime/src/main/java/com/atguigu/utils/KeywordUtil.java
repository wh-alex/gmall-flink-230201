package com.atguigu.utils;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class KeywordUtil {

    public static List<String> splitKeyword(String keyword) throws IOException {

        ArrayList<String> list = new ArrayList<>();

        //创建IK分词对象 true -> ik_smart  false -> ik_max_word
        IKSegmenter ikSegmenter = new IKSegmenter(new StringReader(keyword), false);

        Lexeme next = ikSegmenter.next();
        while (next != null) {
            String word = next.getLexemeText();
            list.add(word);

            next = ikSegmenter.next();
        }

        return list;
    }

    public static void main(String[] args) throws IOException {

        System.out.println(splitKeyword("尚硅谷大数据之Flink实时数仓"));

    }

}
