package com.atguigu.utils;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.Constant;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

public class HBaseUtil {

    private static Logger logger = LoggerFactory.getLogger("aa");

    //获取连接
    public static Connection getConnection() throws IOException {
        Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", Constant.ZK_SERVERS);
        return ConnectionFactory.createConnection(configuration);
    }

    //获取异步连接
    public static AsyncConnection getAsyncConnection() throws Exception, InterruptedException {
        Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", Constant.ZK_SERVERS);
        return ConnectionFactory.createAsyncConnection(configuration).get();
    }

    //关闭连接
    public static void closeConnection(Connection connection) {
        if (connection != null && !connection.isClosed()) {
            try {
                connection.close();
            } catch (IOException ignored) {
            }
        }
    }

    //判断表是否存在
    public static boolean isTableExists(Connection connection, String nameSpace, String table) throws IOException {
        //获取Admin对象
        Admin admin = connection.getAdmin();
        //判断表是否存在
        boolean exists = admin.tableExists(TableName.valueOf(nameSpace, table));
        //释放资源
        admin.close();
        return exists;
    }

    //建表
    public static void createTable(Connection connection, String nameSpace, String table, byte[][] extend, String... familys) throws IOException {

        if (familys.length <= 0) {
            throw new RuntimeException("没有设置列族信息！");
            //System.out.println("没有设置列族信息!");
        }

        //获取Admin对象
        Admin admin = connection.getAdmin();

        //判断表是否存在
        if (!admin.tableExists(TableName.valueOf(nameSpace, table))) {
            //创建表描述器
            TableDescriptorBuilder tableDescriptorBuilder = TableDescriptorBuilder
                    .newBuilder(TableName.valueOf(nameSpace + ":" + table));

            //遍历列族信息,将其设置进表描述器
            for (String family : familys) {
                tableDescriptorBuilder.setColumnFamily(
                        ColumnFamilyDescriptorBuilder
                                .newBuilder(family.getBytes())
                                .build()
                );
            }

            TableDescriptor tableDescriptor = tableDescriptorBuilder.build();

            if (extend == null) {
                admin.createTable(tableDescriptor);
            } else {
                admin.createTable(tableDescriptor, extend);
            }
        } else {
            System.out.println("待创建的表：" + table + "已经存在！");
        }

        //释放资源
        admin.close();
    }

    //删表
    public static void deleteTable(Connection connection, String nameSpace, String table) throws IOException {

        //获取Admin对象
        Admin admin = connection.getAdmin();

        //判断表是否存在
        TableName tableName = TableName.valueOf(nameSpace, table);
        if (admin.tableExists(tableName)) {
            admin.disableTable(tableName);
            admin.deleteTable(tableName);
        } else {
            System.out.println("待删除的表：" + table + "不存在！");
        }

        admin.close();
    }

    //插入数据
    public static void putData(Connection connection, String nameSpace, String tableName, String rowKey, String cf, JSONObject data) throws IOException {

        //获取表对象
        Table table = connection.getTable(TableName.valueOf(nameSpace, tableName));

        //构建Put对象
        Put put = new Put(rowKey.getBytes());

        //循环添加列信息
        Set<Map.Entry<String, Object>> entries = data.entrySet();
        for (Map.Entry<String, Object> entry : entries) {
            Object value1 = entry.getValue();
            if (value1 != null) {
                String value = value1.toString();
                put.addColumn(cf.getBytes(), entry.getKey().getBytes(), value.getBytes());
            }
        }

        table.put(put);

        table.close();
    }

    //删除数据
    public static void deleteData(Connection connection, String nameSpace, String tableName, String rowKey) throws IOException {

        //获取表对象
        Table table = connection.getTable(TableName.valueOf(nameSpace, tableName));

        //创建Delete对象
        Delete delete = new Delete(rowKey.getBytes());

        //执行删除操作,删除整个RowKey
        table.delete(delete);

        //释放资源
        table.close();
    }

    /**
     * @param rowKey 1001
     * @param extend a|,b|
     * @return a_1001 / b_1001 / b|1001
     */
    public static String getRowKey(String rowKey, String extend) {

        //[a|,b|]
        String[] split = extend.split(",");
        ArrayList<String> list = new ArrayList<>();
        for (String s : split) {
            list.add(s.replace("|", "_"));
        }
        list.add(split[split.length - 1]);

        int i = rowKey.hashCode() % list.size();

        return list.get(i) + rowKey;
    }

    public static JSONObject getData(Connection connection, String tableName, String rowKey) throws IOException {

        //获取表对象
        Table table = connection.getTable(TableName.valueOf(Constant.HBASE_NAME_SPACE + ":" + tableName));

        //创建Get对象
        Get get = new Get(rowKey.getBytes());
        Result result = table.get(get);

        //解析
        JSONObject jsonObject = getJsonObject(result);

        //释放资源
        table.close();

        //返回结果
        return jsonObject;
    }

    private static JSONObject getJsonObject(Result result) {
        //创建JSON对象
        JSONObject jsonObject = new JSONObject();

        //处理result,将结果Put进jsonObject
        Cell[] cells = result.rawCells();
        for (Cell cell : cells) {
            jsonObject.put(new String(CellUtil.cloneQualifier(cell)), new String(CellUtil.cloneValue(cell)));
        }

        return jsonObject;
    }

    public static JSONObject getDataByAsync(AsyncConnection hbaseAsyncConnection,
                                            String table,
                                            String rowKey) throws ExecutionException, InterruptedException {
        //获取表对象
        AsyncTable<AdvancedScanResultConsumer> asyncTable = hbaseAsyncConnection.getTable(TableName.valueOf(Constant.HBASE_NAME_SPACE, table));

        //执行查询操作
        Get get = new Get(rowKey.getBytes());
        Result result = asyncTable.get(get).get();

        //解析
        return getJsonObject(result);
    }

    public static String getHBaseDicDDL() {
        return "CREATE TABLE base_dic (\n" +
                " rowkey string,\n" +
                " info ROW<dic_name string>,\n" +
                " PRIMARY KEY (rowkey) NOT ENFORCED\n" +
                ") WITH (\n" +
                " 'connector' = 'hbase-2.2',\n" +
                " 'table-name' = 'gmall_230201:dim_base_dic',\n" +
                " 'zookeeper.quorum' = '" + Constant.ZK_SERVERS + "'\n" +
                ")";
    }

    public static void main(String[] args) throws IOException {
        Connection connection = getConnection();
        //createTable(connection, "default", "test_20230201", null, "f1", "f2");

        //deleteTable(connection, "default", "test_20230201");

//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("name", "zs");
//        jsonObject.put("sex", "male");
//        jsonObject.put("addr", "sh");
//
//        putData(connection, "gmall_230201", "dim_base_trademark", "1001", "f1", jsonObject);
        //deleteData(connection, "gmall_230201", "dim_base_trademark", "1001");
        //System.out.println(getRowKey("1002", "a|,b|"));

        long start = System.currentTimeMillis();
        System.out.println(getData(connection, "dim_sku_info", "1"));
        long end1 = System.currentTimeMillis();
        System.out.println(getData(connection, "dim_sku_info", "1"));
        long end2 = System.currentTimeMillis();

        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();

        System.out.println(end1 - start); //1105   1085   1090
        System.out.println(end2 - end1);  //4      4      3

        connection.close();
    }
}
