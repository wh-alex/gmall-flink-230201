package com.atguigu.utils;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 操作Redis的工具类
 */
public class JedisUtil {

    private static JedisPool jedisPool;

    static {
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(100);
        poolConfig.setMaxIdle(5);
        poolConfig.setMinIdle(5);
        poolConfig.setBlockWhenExhausted(true);
        poolConfig.setMaxWaitMillis(2000);
        poolConfig.setTestOnBorrow(true);

        jedisPool = new JedisPool(poolConfig, "hadoop102", 6379, 10000);
    }

    public static JedisPool getJedisPool() {
        return jedisPool;
    }

    public static Jedis getJedis() {
        System.out.println("~~~获取Jedis客户端~~~");
        return jedisPool.getResource();
    }

    /**
     * 获取一个到 redis 线程安全的异步连接, key value 都用 utf-8 进行编码
     */
    public static StatefulRedisConnection<String, String> getAsyncRedisConnection() {
        // 连接到 redis 的4号库
        RedisClient redisClient = RedisClient.create("redis://hadoop102:6379");
        return redisClient.connect();
    }


    public static void main(String[] args) {
        Jedis jedis = getJedis();
        String pong = jedis.ping();

        jedis.close();
        System.out.println(pong);
    }
}
