package com.atguigu.utils;

import org.apache.hadoop.hbase.client.Connection;

import java.io.IOException;
import java.util.LinkedList;

public class MyHBaseDataSource {

    private LinkedList<Connection> connectionLinkedList = new LinkedList<>();

    public MyHBaseDataSource(int poolSize) throws IOException {
        for (int i = 0; i < poolSize; i++) {
            connectionLinkedList.add(HBaseUtil.getConnection());
        }
    }

    public Connection getConnection() throws IOException {
        if (connectionLinkedList.size() > 0) {
            synchronized (MyHBaseDataSource.class) {
                if (connectionLinkedList.size() > 0) {
                    return connectionLinkedList.removeFirst();
                } else {
                    return HBaseUtil.getConnection();
                }
            }
        } else {
            return HBaseUtil.getConnection();
        }
    }

    public void addBack(Connection connection) {
        connectionLinkedList.add(connection);
    }

    public static void main(String[] args) throws IOException {

        MyHBaseDataSource myHBaseDataSource = new MyHBaseDataSource(5);

        for (int i = 0; i < 6; i++) {
            Connection connection = myHBaseDataSource.getConnection();
            System.out.println(connection);
            //myHBaseDataSource.addBack(connection);
        }

    }

}
