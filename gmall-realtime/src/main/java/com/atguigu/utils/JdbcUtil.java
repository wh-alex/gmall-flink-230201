package com.atguigu.utils;

import com.atguigu.bean.TableProcess;
import com.atguigu.common.Constant;
import com.google.common.base.CaseFormat;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * select count(*) from t;                         单行单列
 * select * from t where id = 'xxx'; id为主键       单行多列
 * select id from t;                               多行单列
 * select * from t;                                多行多列
 * 任何JDBC框架的任何查询
 */
public class JdbcUtil {

    public static <T> List<T> queryList(Connection connection, String querySql, Class<T> tClass, boolean isUnderScoreToCamel) throws SQLException, InstantiationException, IllegalAccessException, InvocationTargetException {

        //创建集合用于存放结果数据
        ArrayList<T> list = new ArrayList<>();

        //预编译SQL
        PreparedStatement preparedStatement = connection.prepareStatement(querySql);

        //执行查询
        ResultSet resultSet = preparedStatement.executeQuery();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        //遍历结果集,将每行数据封装为T对象,放入集合
        while (resultSet.next()) {

            //创建T对象
            T t = tClass.newInstance();

            for (int i = 1; i < columnCount + 1; i++) {
                String columnName = metaData.getColumnName(i);

                if (isUnderScoreToCamel) {
                    columnName = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, columnName.toLowerCase());
                }

                Object value = resultSet.getObject(i);

                //将数据设置进T对象
                BeanUtils.setProperty(t, columnName, value);
            }

            //将T对象放入集合
            list.add(t);
        }

        //释放资源
        resultSet.close();
        preparedStatement.close();

        //返回结果数据
        return list;
    }

    public static void main(String[] args) throws Exception {

        //加载驱动
        Class.forName(Constant.MYSQL_DRIVER);
        //获取连接
        Connection connection = DriverManager.getConnection(Constant.MYSQL_URL, "root", "000000");

        //查询数据
        List<TableProcess> list = queryList(connection,
                "select * from table_process",
                TableProcess.class,
                true);

        for (TableProcess jsonObject : list) {
            System.out.println(jsonObject);
        }

        connection.close();
    }
}
