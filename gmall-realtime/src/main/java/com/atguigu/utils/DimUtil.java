package com.atguigu.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;
import org.apache.hadoop.hbase.client.AsyncConnection;
import org.apache.hadoop.hbase.client.Connection;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class DimUtil {

    public static JSONObject getDimInfo(Jedis jedis, Connection connection, String table, String rowKey) throws IOException {

        //查询Redis
        String redisKey = "DIM:" + table + ":" + rowKey;
        String dimInfoStr = jedis.get(redisKey);
        if (dimInfoStr != null) {
            //查到则直接返回
            jedis.expire(redisKey, 24 * 3600);
            return JSON.parseObject(dimInfoStr);
        }
        //查不到则查询HBase
        JSONObject dimInfo = HBaseUtil.getData(connection, table, rowKey);

        //将从HBase查到的数据写入Redis
        jedis.setex(redisKey, 24 * 3600, dimInfo.toJSONString());

        //返回结果
        return dimInfo;
    }

    public static JSONObject getDimInfoByAsync(RedisAsyncCommands<String, String> asyncCommands,
                                               AsyncConnection hbaseAsyncConnection,
                                               String table,
                                               String rowKey) throws ExecutionException, InterruptedException {

        //先查询Redis数据
        String redisKey = "DIM:" + table + ":" + rowKey;
        String dimInfoStr = asyncCommands.get(redisKey).get();
        if (dimInfoStr != null) {
            asyncCommands.expire(redisKey, 24 * 3600);
            return JSON.parseObject(dimInfoStr);
        }

        //再查询HBase数据
        JSONObject dimInfo = HBaseUtil.getDataByAsync(hbaseAsyncConnection, table, rowKey);

        //将数据写回到Redis
        asyncCommands.setex(redisKey, 24 * 3600, dimInfo.toJSONString());

        //返回结果
        return dimInfo;
    }

    public static void main(String[] args) throws Exception {

//        Jedis jedis = JedisUtil.getJedis();
//        Connection connection = HBaseUtil.getConnection();
//        long start = System.currentTimeMillis();
//        System.out.println(getDimInfo(jedis, connection, "dim_base_trademark", "12"));
//        long end1 = System.currentTimeMillis();
//        System.out.println(getDimInfo(jedis, connection, "dim_base_trademark", "12"));
//        long end2 = System.currentTimeMillis();
//        System.out.println(end1 - start);
//        System.out.println(end2 - end1);//0 1 0 1
//        connection.close();
//        jedis.close();

        AsyncConnection asyncConnection = HBaseUtil.getAsyncConnection();
        StatefulRedisConnection<String, String> asyncRedisConnection = JedisUtil.getAsyncRedisConnection();
        RedisAsyncCommands<String, String> asyncCommands = asyncRedisConnection.async();

        System.out.println(getDimInfoByAsync(asyncCommands,
                asyncConnection,
                "dim_base_trademark",
                "11"));

        asyncConnection.close();
        //asyncCommands.shutdown(true); //关闭Redis服务
        asyncRedisConnection.close();
    }

}
