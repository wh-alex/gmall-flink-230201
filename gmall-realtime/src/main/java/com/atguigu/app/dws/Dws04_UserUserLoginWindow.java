package com.atguigu.app.dws;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.atguigu.bean.UserLoginBean;
import com.atguigu.common.Constant;
import com.atguigu.utils.DateFormatUtil;
import com.atguigu.utils.DorisUtil;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;

//数据流：web/app -> 日志服务器(log文件) -> Flume -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> Doris(DWS)
//程  序：Mock -> log文件 -> Flume(f1.sh) -> Kafka(ZK) -> DwdTrafficBaseLogSplit -> Kafka(ZK) -> Dws04_UserUserLoginWindow(CK HDFS) -> Doris
public class Dws04_UserUserLoginWindow {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //CheckPoint
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        env.enableCheckpointing(5000L);
        env.setStateBackend(new HashMapStateBackend());
        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dws/keyword");
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
        checkpointConfig.setCheckpointTimeout(10000L);

        //固定延迟重启
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3, 1000L));

        //TODO 2.读取Kafka DWD层页面日志主题数据
        DataStreamSource<String> kafkaDS = env.fromSource(KafkaUtil.getKafkaSource(Constant.TOPIC_DWD_TRAFFIC_PAGE, "home_detail_230201"), WatermarkStrategy.noWatermarks(), "kafka-source");

        //TODO 3.将数据转换为JSON对象并过滤出首页和商品详情页数据
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                JSONObject jsonObject = JSON.parseObject(value);
                String lastPageId = jsonObject.getJSONObject("page").getString("last_page_id");
                String uid = jsonObject.getJSONObject("common").getString("uid");

                if (uid != null && (lastPageId == null || "login".equals(lastPageId))) {
                    out.collect(jsonObject);
                }
            }
        });

        //TODO 4.按照Uid进行分组,去重 并将其转换为JavaBean对象
        SingleOutputStreamOperator<UserLoginBean> userLoginDS = jsonObjDS.keyBy(json -> json.getJSONObject("common").getString("uid"))
                .flatMap(new RichFlatMapFunction<JSONObject, UserLoginBean>() {

                    private ValueState<String> lastDtState;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        lastDtState = getRuntimeContext().getState(new ValueStateDescriptor<String>("last-dt", String.class));
                    }

                    @Override
                    public void flatMap(JSONObject value, Collector<UserLoginBean> out) throws Exception {

                        //取出相应信息
                        String lastDt = lastDtState.value();
                        Long ts = value.getLong("ts");
                        String curDt = DateFormatUtil.toDate(ts);

                        //定义用户数
                        long uv = 0L;
                        long backUv = 0L;

                        if (lastDt == null) {
                            uv = 1L;
                            lastDtState.update(curDt);
                        } else if (!lastDt.equals(curDt)) {
                            uv = 1L;
                            lastDtState.update(curDt);

                            //判断两次登录之间是否超过7天
                            if ((DateFormatUtil.toTs(curDt) - DateFormatUtil.toTs(lastDt)) / (24 * 3600 * 1000L) > 7) {
                                backUv = 1L;
                            }
                        }

                        if (uv == 1L) {
                            out.collect(new UserLoginBean("", "",
                                    curDt,
                                    backUv,
                                    uv,
                                    ts));
                        }
                    }
                });

        //TODO 5.开窗、聚合
        SingleOutputStreamOperator<UserLoginBean> reduceDS = userLoginDS.assignTimestampsAndWatermarks(WatermarkStrategy.<UserLoginBean>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<UserLoginBean>() {
                    @Override
                    public long extractTimestamp(UserLoginBean element, long recordTimestamp) {
                        return element.getTs();
                    }
                })).windowAll(TumblingEventTimeWindows.of(Time.seconds(10)))
                .reduce(new ReduceFunction<UserLoginBean>() {
                    @Override
                    public UserLoginBean reduce(UserLoginBean value1, UserLoginBean value2) throws Exception {
                        value1.setUuCt(value1.getUuCt() + value2.getUuCt());
                        value1.setBackCt(value1.getBackCt() + value2.getBackCt());
                        return value1;
                    }
                }, new AllWindowFunction<UserLoginBean, UserLoginBean, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow window, Iterable<UserLoginBean> values, Collector<UserLoginBean> out) throws Exception {

                        UserLoginBean next = values.iterator().next();

                        next.setEdt(DateFormatUtil.toYmdHms(window.getEnd()));
                        next.setStt(DateFormatUtil.toYmdHms(window.getStart()));

                        out.collect(next);
                    }
                });

        //TODO 6.将数据写出
        reduceDS.print(">>>>");
        reduceDS.map(bean -> {
                    SerializeConfig config = new SerializeConfig();
                    config.propertyNamingStrategy = PropertyNamingStrategy.SnakeCase;  // 转成json的时候, 属性名使用下划线
                    return JSON.toJSONString(bean, config);
                })
                .sinkTo(DorisUtil.getDorisSink("gmall_230201.dws_user_user_login_window"));

        //TODO 7.启动
        env.execute();

    }
}
