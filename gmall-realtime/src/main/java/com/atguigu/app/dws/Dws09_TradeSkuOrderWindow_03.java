package com.atguigu.app.dws;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.atguigu.app.func.AsyncDimFunction2;
import com.atguigu.bean.TradeSkuOrderBean;
import com.atguigu.common.Constant;
import com.atguigu.utils.DorisUtil;
import com.atguigu.utils.KafkaUtil;
import com.atguigu.utils.WindowFunctionUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.StateTtlConfig;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

//数据流：web/app -> 业务数据库(MySQL) -> Maxwell -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> Doris(DWS)
//程  序：Mock -> 业务数据库(MySQL) -> Maxwell -> Kafka(ZK) -> DwdTradeOrderDetail -> Kafka(ZK) -> Dws09_TradeSkuOrderWindow(HDFS HBase Redis) -> Doris
public class Dws09_TradeSkuOrderWindow_03 {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //CheckPoint
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        env.enableCheckpointing(5000L);
        env.setStateBackend(new HashMapStateBackend());
        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dws/keyword");
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
        checkpointConfig.setCheckpointTimeout(10000L);

        //TODO 2.读取Kafka DWD层下单明细主题数据
        DataStreamSource<String> kafkaDS = env.fromSource(KafkaUtil.getKafkaSource(Constant.TOPIC_DWD_TRADE_ORDER_DETAIL, "sku_order_window_230201"), WatermarkStrategy.noWatermarks(), "kafka-source");

        //TODO 3.过滤null值并转换为JSON对象
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                if (value != null) {
                    out.collect(JSON.parseObject(value));
                }
            }
        });

        //TODO 4.按照订单明细ID进行分组,去重(left join),同时转换为JavaBean对象
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderBeanDS = jsonObjDS.keyBy(json -> json.getString("id"))
                .flatMap(new RichFlatMapFunction<JSONObject, TradeSkuOrderBean>() {

                    private ValueState<String> existState;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        StateTtlConfig ttlConfig = new StateTtlConfig.Builder(Time.seconds(5))
                                .setUpdateType(StateTtlConfig.UpdateType.OnReadAndWrite)
                                .build();
                        ValueStateDescriptor<String> stateDescriptor = new ValueStateDescriptor<>("state", String.class);
                        stateDescriptor.enableTimeToLive(ttlConfig);

                        existState = getRuntimeContext().getState(stateDescriptor);
                    }

                    @Override
                    public void flatMap(JSONObject value, Collector<TradeSkuOrderBean> out) throws Exception {
                        //取出状态中的值
                        String state = existState.value();
                        if (state == null) {

                            BigDecimal splitOriginalAmount = value.getBigDecimal("split_original_amount");
                            //System.out.println("splitOriginalAmount>>" + splitOriginalAmount);

                            existState.update("1");
                            out.collect(TradeSkuOrderBean.builder()
                                    .skuId(value.getString("sku_id"))
                                    .skuName(value.getString("sku_name"))
                                    .curDate(value.getString("create_time").split(" ")[0])
                                    .originalAmount(splitOriginalAmount)
                                    .activityAmount(value.getBigDecimal("split_activity_amount"))
                                    .couponAmount(value.getBigDecimal("split_coupon_amount"))
                                    .orderAmount(value.getBigDecimal("split_total_amount"))
                                    .ts(value.getLong("create_time"))
                                    .build());
                        }
                    }
                });

        //TODO 5.提取时间戳,分组开窗聚合
        SingleOutputStreamOperator<TradeSkuOrderBean> reduceDS = tradeSkuOrderBeanDS.assignTimestampsAndWatermarks(WatermarkStrategy.<TradeSkuOrderBean>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<TradeSkuOrderBean>() {
                    @Override
                    public long extractTimestamp(TradeSkuOrderBean element, long recordTimestamp) {
                        return element.getTs();
                    }
                })).keyBy(TradeSkuOrderBean::getSkuId)
                .window(TumblingEventTimeWindows.of(org.apache.flink.streaming.api.windowing.time.Time.seconds(10)))
                .reduce(new ReduceFunction<TradeSkuOrderBean>() {
                    @Override
                    public TradeSkuOrderBean reduce(TradeSkuOrderBean value1, TradeSkuOrderBean value2) throws Exception {
                        value1.setOriginalAmount(value1.getOriginalAmount().add(value2.getOriginalAmount()));
                        value1.setActivityAmount(value1.getActivityAmount().add(value2.getActivityAmount()));
                        value1.setCouponAmount(value1.getCouponAmount().add(value2.getCouponAmount()));
                        value1.setOrderAmount(value1.getOrderAmount().add(value2.getOrderAmount()));
                        return value1;
                    }
                }, new WindowFunction<TradeSkuOrderBean, TradeSkuOrderBean, String, TimeWindow>() {
                    @Override
                    public void apply(String key, TimeWindow window, Iterable<TradeSkuOrderBean> input, Collector<TradeSkuOrderBean> out) throws Exception {
//                        TradeSkuOrderBean next = input.iterator().next();
//                        next.setEdt(DateFormatUtil.toYmdHms(window.getEnd()));
//                        next.setStt(DateFormatUtil.toYmdHms(window.getStart()));
//                        out.collect(next);
                        WindowFunctionUtil.setSttAndEdt(window, input, out);
                    }
                });

        reduceDS.print("reduceDS>>>>");

        //TODO 6.关联维表
        //6.1 dim_sku_info
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderBeanWithSkuDS = AsyncDataStream.unorderedWait(reduceDS,
                new AsyncDimFunction2<TradeSkuOrderBean>("dim_sku_info") {
                    @Override
                    public String getRowKey(TradeSkuOrderBean value) {
                        return value.getSkuId();
                    }

                    @Override
                    public void join(TradeSkuOrderBean value, JSONObject dimInfo) {
                        value.setSpuId(dimInfo.getString("spu_id"));
                        value.setTrademarkId(dimInfo.getString("tm_id"));
                        value.setCategory3Id(dimInfo.getString("category3_id"));
                    }
                },
                60, TimeUnit.SECONDS);

        //6.2 dim_spu_info
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderBeanWithSpuDS = AsyncDataStream.unorderedWait(tradeSkuOrderBeanWithSkuDS,
                new AsyncDimFunction2<TradeSkuOrderBean>("dim_spu_info") {
                    @Override
                    public String getRowKey(TradeSkuOrderBean value) {
                        return value.getSpuId();
                    }

                    @Override
                    public void join(TradeSkuOrderBean value, JSONObject dimInfo) {
                        value.setSpuName(dimInfo.getString("spu_name"));
                    }
                },
                60, TimeUnit.SECONDS);

        //6.3 dim_base_trademark
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderBeanWithTmDS = AsyncDataStream.unorderedWait(tradeSkuOrderBeanWithSpuDS,
                new AsyncDimFunction2<TradeSkuOrderBean>("dim_base_trademark") {
                    @Override
                    public String getRowKey(TradeSkuOrderBean value) {
                        return value.getTrademarkId();
                    }

                    @Override
                    public void join(TradeSkuOrderBean value, JSONObject dimInfo) {
                        value.setTrademarkName(dimInfo.getString("tm_name"));
                    }
                },
                60, TimeUnit.SECONDS);

        //6.4 dim_base_category3
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderBeanWithC3DS = AsyncDataStream.unorderedWait(tradeSkuOrderBeanWithTmDS,
                new AsyncDimFunction2<TradeSkuOrderBean>("dim_base_category3") {
                    @Override
                    public String getRowKey(TradeSkuOrderBean value) {
                        return value.getCategory3Id();
                    }

                    @Override
                    public void join(TradeSkuOrderBean value, JSONObject dimInfo) {
                        value.setCategory3Name(dimInfo.getString("name"));
                        value.setCategory2Id(dimInfo.getString("category2_id"));
                    }
                },
                60, TimeUnit.SECONDS);

        //6.5 dim_base_category2
        SingleOutputStreamOperator<TradeSkuOrderBean> tradeSkuOrderBeanWithC2DS = AsyncDataStream.unorderedWait(tradeSkuOrderBeanWithC3DS,
                new AsyncDimFunction2<TradeSkuOrderBean>("dim_base_category2") {
                    @Override
                    public String getRowKey(TradeSkuOrderBean value) {
                        return value.getCategory2Id();
                    }

                    @Override
                    public void join(TradeSkuOrderBean value, JSONObject dimInfo) {
                        value.setCategory2Name(dimInfo.getString("name"));
                        value.setCategory1Id(dimInfo.getString("category1_id"));
                    }
                },
                60, TimeUnit.SECONDS);

        //6.6 dim_base_category1
        SingleOutputStreamOperator<TradeSkuOrderBean> resultDS = AsyncDataStream.unorderedWait(
                tradeSkuOrderBeanWithC2DS,
                new AsyncDimFunction2<TradeSkuOrderBean>("dim_base_category1") {
                    @Override
                    public String getRowKey(TradeSkuOrderBean value) {
                        return value.getCategory1Id();
                    }

                    @Override
                    public void join(TradeSkuOrderBean value, JSONObject dimInfo) {
                        value.setCategory1Name(dimInfo.getString("name"));
                    }
                },
                60, TimeUnit.SECONDS);

        resultDS.print("resultDS-------------");

        //TODO 7.写出到Doris
        resultDS.map(bean -> {
                    SerializeConfig config = new SerializeConfig();
                    config.propertyNamingStrategy = PropertyNamingStrategy.SnakeCase;  // 转成json的时候, 属性名使用下划线
                    return JSON.toJSONString(bean, config);
                })
                .sinkTo(DorisUtil.getDorisSink("gmall_230201.dws_trade_sku_order_window"));


        //TODO 8.启动
        env.execute("Dws09_TradeSkuOrderWindow");

    }

}
