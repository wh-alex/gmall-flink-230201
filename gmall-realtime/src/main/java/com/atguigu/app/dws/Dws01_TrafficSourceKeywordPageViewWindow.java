package com.atguigu.app.dws;

import com.atguigu.app.func.SplitFunction;
import com.atguigu.common.Constant;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

//数据流：web/app -> 日志服务器(log文件) -> Flume -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> Doris(DWS)
//程  序：Mock -> log文件 -> Flume(f1.sh) -> Kafka(ZK) -> DwdTrafficBaseLogSplit -> Kafka(ZK) -> Dws01_TrafficSourceKeywordPageViewWindow -> Doris
public class Dws01_TrafficSourceKeywordPageViewWindow {

    public static void main(String[] args) {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //CheckPoint
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        env.enableCheckpointing(5000L);
        env.setStateBackend(new HashMapStateBackend());
        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dws/keyword");
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
        checkpointConfig.setCheckpointTimeout(10000L);

        //TODO 2.读取Kafka 页面日志主题
        tableEnv.executeSql("" +
                "create table page_view( " +
                "    `page` map<string,string>, " +
                "    `ts` bigint, " +
                "    `rt` as TO_TIMESTAMP_LTZ(ts, 3), " +
                "    WATERMARK FOR `rt` AS `rt` - INTERVAL '2' SECOND " +
                ")" + KafkaUtil.getKafkaDDL(Constant.TOPIC_DWD_TRAFFIC_PAGE, "key_word_page_view"));

        //TODO 3.过滤出搜索日志
        Table filterTable = tableEnv.sqlQuery("" +
                "select " +
                "    `page`['item'] item, " +
                "    rt " +
                "from page_view " +
                "where `page`['last_page_id'] = 'search' " +
                "and `page`['item_type'] = 'keyword' " +
                "and `page`['item'] is not null");
        tableEnv.createTemporaryView("filter_table", filterTable);

        //TODO 4.自定义UDTF并注册
        tableEnv.createTemporarySystemFunction("SplitFunction", SplitFunction.class);

        //TODO 5.使用UDTF进行分词处理
        Table splitTable = tableEnv.sqlQuery("" +
                "SELECT  " +
                "    rt,  " +
                "    word " +
                "FROM filter_table, LATERAL TABLE(SplitFunction(item))");
        tableEnv.createTemporaryView("split_table", splitTable);

        //TODO 6.分组开窗、聚合
//        tableEnv.sqlQuery("" +
//                "SELECT  " +
//                "    window_start,  " +
//                "    window_end,  " +
//                "    word, " +
//                "    count(*) ct " +
//                "FROM TABLE( " +
//                "    TUMBLE(TABLE split_table, DESCRIPTOR(rt), INTERVAL '10' SECOND)) " +
//                "GROUP BY word,window_start, window_end");

        Table resultTable = tableEnv.sqlQuery("" +
                "SELECT " +
                "    DATE_FORMAT(TUMBLE_START(rt, INTERVAL '10' SECOND),'yyyy-MM-dd HH:mm:ss') AS stt, " +
                "    DATE_FORMAT(TUMBLE_END(rt, INTERVAL '10' SECOND),'yyyy-MM-dd HH:mm:ss') AS edt, " +
                "    word keyword, " +
                "    DATE_FORMAT(TUMBLE_START(rt, INTERVAL '10' SECOND),'yyyy-MM-dd') AS cur_date, " +
                "    count(*) keyword_count " +
                "    FROM split_table " +
                "GROUP BY " +
                "    TUMBLE(rt, INTERVAL '10' SECOND), " +
                "    word");
        tableEnv.createTemporaryView("result_table", resultTable);

//        tableEnv.sqlQuery("select * from result_table")
//                .execute()
//                .print();

        //TODO 7.将数据写入Doris
        tableEnv.executeSql("CREATE table doris_t(  " +
                " stt string, " +
                " edt string, " +
                " keyword string, " +
                " cur_date string, " +
                " keyword_count bigint " +
                ")WITH (" +
                "  'connector' = 'doris', " +
                "  'fenodes' = 'hadoop102:7030', " +
                " 'table.identifier' = 'gmall_230201.dws_traffic_source_keyword_page_view_window', " +
                "  'username' = 'root', " +
                "  'password' = '000000', " +
                "  'sink.properties.format' = 'json', " +
                "  'sink.properties.read_json_by_line' = 'true', " +
                "  'sink.buffer-count' = '4', " +
                "  'sink.buffer-size' = '4086'," +
                "  'sink.enable-2pc' = 'false' " + // 测试阶段可以关闭两阶段提交,方便测试
                ") ");
        tableEnv.executeSql("insert into doris_t select * from result_table");


    }

}
