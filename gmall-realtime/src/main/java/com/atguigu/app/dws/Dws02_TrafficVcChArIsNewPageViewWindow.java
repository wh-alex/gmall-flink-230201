package com.atguigu.app.dws;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TrafficPageViewBean;
import com.atguigu.common.Constant;
import com.atguigu.utils.DateFormatUtil;
import com.atguigu.utils.DorisUtil;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.state.*;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;

//数据流：web/app -> 日志服务器(log文件) -> Flume -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> Doris(DWS)
//程  序：Mock -> log文件 -> Flume(f1.sh) -> Kafka(ZK) -> DwdTrafficBaseLogSplit -> Kafka(ZK) -> Dws02_TrafficVcChArIsNewPageViewWindow -> Doris
public class Dws02_TrafficVcChArIsNewPageViewWindow {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //CheckPoint
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        env.enableCheckpointing(5000L);
        env.setStateBackend(new HashMapStateBackend());
        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dws/keyword");
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
        checkpointConfig.setCheckpointTimeout(10000L);

        //固定延迟重启
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3, 1000L));

        //TODO 2.读取Kafka 页面日志主题数据
        DataStreamSource<String> kafkaDS = env.fromSource(KafkaUtil.getKafkaSource(Constant.TOPIC_DWD_TRAFFIC_PAGE, "page_view_230201"), WatermarkStrategy.noWatermarks(), "kafka-source");

        //TODO 3.将数据转换为JSON对象
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.map(JSON::parseObject);

        //TODO 4.按照Mid进行分组,去重 mid  sid,
        KeyedStream<JSONObject, String> keyedStream = jsonObjDS.keyBy(json -> json.getJSONObject("common").getString("mid"));
        SingleOutputStreamOperator<TrafficPageViewBean> trafficPageViewBeanDS = keyedStream.map(new RichMapFunction<JSONObject, TrafficPageViewBean>() {

            private ValueState<String> lastDtState;
            private ListState<String> sidState;

            @Override
            public void open(Configuration parameters) throws Exception {
                ValueStateDescriptor<String> valueStateDescriptor = new ValueStateDescriptor<>("value-state", String.class);
                StateTtlConfig valueTtlConfig = new StateTtlConfig.Builder(Time.days(1))
                        .setUpdateType(StateTtlConfig.UpdateType.OnReadAndWrite)
                        .build();
                valueStateDescriptor.enableTimeToLive(valueTtlConfig);

                ListStateDescriptor<String> listStateDescriptor = new ListStateDescriptor<>("list-state", String.class);
                StateTtlConfig listTtlConfig = new StateTtlConfig.Builder(Time.minutes(1))
                        .setUpdateType(StateTtlConfig.UpdateType.OnReadAndWrite)
                        .build();
                listStateDescriptor.enableTimeToLive(listTtlConfig);

                lastDtState = getRuntimeContext().getState(valueStateDescriptor);
                sidState = getRuntimeContext().getListState(listStateDescriptor);
            }

            @Override
            public TrafficPageViewBean map(JSONObject value) throws Exception {

                //取出相关数据
                JSONObject common = value.getJSONObject("common");
                String lastDt = lastDtState.value();
                Iterable<String> iterable = sidState.get();
                Long ts = value.getLong("ts");
                String curDt = DateFormatUtil.toDate(ts);
                String sid = common.getString("sid");

                long uv = 0L;
                if (lastDt == null || !lastDt.equals(curDt)) {
                    uv = 1L;
                    lastDtState.update(curDt);
                }

                long sv = 1L;
                for (String next : iterable) {
                    if (next.equals(sid)) {
                        sv = 0L;
                        break;
                    }
                }
                sidState.add(sid);

                return new TrafficPageViewBean("", "",
                        common.getString("vc"),
                        common.getString("ch"),
                        common.getString("ar"),
                        common.getString("is_new"),
                        curDt,
                        uv,
                        sv,
                        1L,
                        value.getJSONObject("page").getLong("during_time"),
                        ts);
            }
        });

        //TODO 5.提取事件时间生成WaterMark
        SingleOutputStreamOperator<TrafficPageViewBean> trafficPageViewBeanWithWMDS = trafficPageViewBeanDS.assignTimestampsAndWatermarks(WatermarkStrategy.<TrafficPageViewBean>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<TrafficPageViewBean>() {
            @Override
            public long extractTimestamp(TrafficPageViewBean element, long recordTimestamp) {
                return element.getTs();
            }
        }));

        //TODO 6.分组开窗聚合
        SingleOutputStreamOperator<TrafficPageViewBean> resultDS = trafficPageViewBeanWithWMDS.keyBy(new KeySelector<TrafficPageViewBean, Tuple4<String, String, String, String>>() {
            @Override
            public Tuple4<String, String, String, String> getKey(TrafficPageViewBean value) throws Exception {
                return new Tuple4<>(value.getIs_new(),
                        value.getAr(),
                        value.getCh(),
                        value.getVc());
            }
        }).window(TumblingEventTimeWindows.of(org.apache.flink.streaming.api.windowing.time.Time.seconds(10))).reduce(new ReduceFunction<TrafficPageViewBean>() {
            @Override
            public TrafficPageViewBean reduce(TrafficPageViewBean value1, TrafficPageViewBean value2) throws Exception {
                value1.setDur_sum(value1.getDur_sum() + value2.getDur_sum());
                value1.setSv_ct(value1.getSv_ct() + value2.getSv_ct());
                value1.setPv_ct(value1.getPv_ct() + value2.getPv_ct());
                value1.setUv_ct(value1.getUv_ct() + value2.getUv_ct());
                return value1;
            }
        }, new WindowFunction<TrafficPageViewBean, TrafficPageViewBean, Tuple4<String, String, String, String>, TimeWindow>() {
            @Override
            public void apply(Tuple4<String, String, String, String> key, TimeWindow window, Iterable<TrafficPageViewBean> input, Collector<TrafficPageViewBean> out) throws Exception {
                //取出数据
                TrafficPageViewBean next = input.iterator().next();

                //补充窗口信息
                next.setEdt(DateFormatUtil.toYmdHms(window.getEnd()));
                next.setStt(DateFormatUtil.toYmdHms(window.getStart()));

                out.collect(next);
            }
        });

        //TODO 7.将数据写出到Doris
        resultDS.print(">>>>>");
        resultDS.map(JSON::toJSONString)
                .sinkTo(DorisUtil.getDorisSink("gmall_230201.dws_traffic_vc_ch_ar_is_new_page_view_window"));

        //TODO 8.启动任务
        env.execute();

    }

}
