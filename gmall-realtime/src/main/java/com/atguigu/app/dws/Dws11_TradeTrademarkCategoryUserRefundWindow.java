package com.atguigu.app.dws;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.atguigu.app.func.DimInfoRichMapFunction;
import com.atguigu.bean.TradeTrademarkCategoryUserRefundBean;
import com.atguigu.common.Constant;
import com.atguigu.utils.DateFormatUtil;
import com.atguigu.utils.DorisUtil;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;
import java.util.HashSet;

public class Dws11_TradeTrademarkCategoryUserRefundWindow {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //CheckPoint
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        env.enableCheckpointing(5000L);
        env.setStateBackend(new HashMapStateBackend());
        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dws/keyword");
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
        checkpointConfig.setCheckpointTimeout(10000L);

        //固定延迟重启
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3, 1000L));

        //TODO 2.读取Kafka DWD层退单主题数据
        DataStreamSource<String> kafkaDS = env.fromSource(KafkaUtil.getKafkaSource(Constant.TOPIC_DWD_TRADE_ORDER_REFUND, "trademark_category_user_refund_230201"), WatermarkStrategy.noWatermarks(), "kafka-source");

        //TODO 3.将数据转换为JavaBean对象
        SingleOutputStreamOperator<TradeTrademarkCategoryUserRefundBean> tradeTrademarkCategoryUserRefundDS = kafkaDS.map(new MapFunction<String, TradeTrademarkCategoryUserRefundBean>() {
            @Override
            public TradeTrademarkCategoryUserRefundBean map(String value) throws Exception {
                JSONObject jsonObject = JSON.parseObject(value);
                HashSet<String> orderIds = new HashSet<>();
                orderIds.add(jsonObject.getString("order_id"));
                return TradeTrademarkCategoryUserRefundBean.builder()
                        .skuId(jsonObject.getString("sku_id"))
                        .userId(jsonObject.getString("user_id"))
                        .curDate(jsonObject.getString("create_time").split(" ")[0])
                        .orderIdSet(orderIds)
                        .ts(jsonObject.getLong("create_time"))
                        .build();
            }
        });

        //TODO 4.关联Sku_info表,补充与分组相关的字段 tm_id category3_id
        SingleOutputStreamOperator<TradeTrademarkCategoryUserRefundBean> tradeTrademarkCategoryUserRefundWithSkuDS = tradeTrademarkCategoryUserRefundDS.map(new DimInfoRichMapFunction<TradeTrademarkCategoryUserRefundBean>("dim_sku_info") {
            @Override
            public String getRowKey(TradeTrademarkCategoryUserRefundBean value) {
                return value.getSkuId();
            }

            @Override
            public void join(TradeTrademarkCategoryUserRefundBean value, JSONObject dimInfo) {
                value.setTrademarkId(dimInfo.getString("tm_id"));
                value.setCategory3Id(dimInfo.getString("category3_id"));
            }
        });

        //TODO 5.提取事件戳，分组开窗聚合
        SingleOutputStreamOperator<TradeTrademarkCategoryUserRefundBean> reduceDS = tradeTrademarkCategoryUserRefundWithSkuDS.assignTimestampsAndWatermarks(WatermarkStrategy.<TradeTrademarkCategoryUserRefundBean>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<TradeTrademarkCategoryUserRefundBean>() {
                    @Override
                    public long extractTimestamp(TradeTrademarkCategoryUserRefundBean element, long recordTimestamp) {
                        return element.getTs();
                    }
                })).keyBy(new KeySelector<TradeTrademarkCategoryUserRefundBean, Tuple3<String, String, String>>() {
                    @Override
                    public Tuple3<String, String, String> getKey(TradeTrademarkCategoryUserRefundBean value) throws Exception {
                        return new Tuple3<>(value.getCategory3Id(), value.getTrademarkId(), value.getUserId());
                    }
                }).window(TumblingEventTimeWindows.of(Time.seconds(10)))
                .reduce(new ReduceFunction<TradeTrademarkCategoryUserRefundBean>() {
                    @Override
                    public TradeTrademarkCategoryUserRefundBean reduce(TradeTrademarkCategoryUserRefundBean value1, TradeTrademarkCategoryUserRefundBean value2) throws Exception {
                        value1.getOrderIdSet().addAll(value2.getOrderIdSet());
                        return value1;
                    }
                }, new WindowFunction<TradeTrademarkCategoryUserRefundBean, TradeTrademarkCategoryUserRefundBean, Tuple3<String, String, String>, TimeWindow>() {
                    @Override
                    public void apply(Tuple3<String, String, String> key, TimeWindow window, Iterable<TradeTrademarkCategoryUserRefundBean> input, Collector<TradeTrademarkCategoryUserRefundBean> out) throws Exception {
                        TradeTrademarkCategoryUserRefundBean next = input.iterator().next();

                        next.setStt(DateFormatUtil.toYmdHms(window.getStart()));
                        next.setEdt(DateFormatUtil.toYmdHms(window.getEnd()));
                        next.setRefundCount((long) next.getOrderIdSet().size());

                        out.collect(next);
                    }
                });

        reduceDS.print("reduceDS>>>>>>");

        //TODO 6.关联维表,补充与分组不相关的维度信息
        SingleOutputStreamOperator<TradeTrademarkCategoryUserRefundBean> reduceWithTmDS = reduceDS.map(new DimInfoRichMapFunction<TradeTrademarkCategoryUserRefundBean>("dim_base_trademark") {
            @Override
            public String getRowKey(TradeTrademarkCategoryUserRefundBean value) {
                return value.getTrademarkId();
            }

            @Override
            public void join(TradeTrademarkCategoryUserRefundBean value, JSONObject dimInfo) {
                value.setTrademarkName(dimInfo.getString("tm_name"));
            }
        });

        SingleOutputStreamOperator<TradeTrademarkCategoryUserRefundBean> reduceWithC3DS = reduceWithTmDS.map(new DimInfoRichMapFunction<TradeTrademarkCategoryUserRefundBean>("dim_base_category3") {
            @Override
            public String getRowKey(TradeTrademarkCategoryUserRefundBean value) {
                return value.getCategory3Id();
            }

            @Override
            public void join(TradeTrademarkCategoryUserRefundBean value, JSONObject dimInfo) {
                value.setCategory3Name(dimInfo.getString("name"));
                value.setCategory2Id(dimInfo.getString("category2_id"));
            }
        });

        SingleOutputStreamOperator<TradeTrademarkCategoryUserRefundBean> reduceWithC2DS = reduceWithC3DS.map(new DimInfoRichMapFunction<TradeTrademarkCategoryUserRefundBean>("dim_base_category2") {
            @Override
            public String getRowKey(TradeTrademarkCategoryUserRefundBean value) {
                return value.getCategory2Id();
            }

            @Override
            public void join(TradeTrademarkCategoryUserRefundBean value, JSONObject dimInfo) {
                value.setCategory2Name(dimInfo.getString("name"));
                value.setCategory1Id(dimInfo.getString("category1_id"));
            }
        });

        SingleOutputStreamOperator<TradeTrademarkCategoryUserRefundBean> resultDS = reduceWithC2DS.map(new DimInfoRichMapFunction<TradeTrademarkCategoryUserRefundBean>("dim_base_category1") {
            @Override
            public String getRowKey(TradeTrademarkCategoryUserRefundBean value) {
                return value.getCategory1Id();
            }

            @Override
            public void join(TradeTrademarkCategoryUserRefundBean value, JSONObject dimInfo) {
                value.setCategory1Name(dimInfo.getString("name"));
            }
        });

        //TODO 7.将数据写出
        resultDS.print("resultDS>>>>>>");
        resultDS.map(bean -> {
                    SerializeConfig config = new SerializeConfig();
                    config.propertyNamingStrategy = PropertyNamingStrategy.SnakeCase;
                    return JSON.toJSONString(bean, config);
                })
                .sinkTo(DorisUtil.getDorisSink("gmall_230201.dws_trade_trademark_category_user_refund_window"));

        //TODO 8.启动
        env.execute("Dws11_TradeTrademarkCategoryUserRefundWindow");

    }

}
