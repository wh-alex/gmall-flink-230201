package com.atguigu.app.dws;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.atguigu.bean.TradePaymentBean;
import com.atguigu.common.Constant;
import com.atguigu.utils.DateFormatUtil;
import com.atguigu.utils.DorisUtil;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;

//数据流：web/app -> 业务数据库(MySQL) -> Maxwell -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> Doris(DWS)
//程  序：Mock -> 业务数据库(MySQL) -> Maxwell -> Kafka(ZK) -> DwdTradeOrderDetail -> Kafka(ZK) -> DwdTradePayDetailSuc -> Kafka(ZK) -> Dws08_TradePaymentSucWindow(Ck HDFS) -> Doris
public class Dws08_TradePaymentSucWindow {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //CheckPoint
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        env.enableCheckpointing(5000L);
        env.setStateBackend(new HashMapStateBackend());
        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dws/keyword");
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
        checkpointConfig.setCheckpointTimeout(10000L);

        //TODO 2.读取Kafka DWD层 支付成功主题数据并转换为JSON对象
        SingleOutputStreamOperator<JSONObject> kafkaDS = env.fromSource(KafkaUtil.getKafkaSource(Constant.TOPIC_DWD_TRADE_PAY_DETAIL_SUC, "payment_suc_window_230201"), WatermarkStrategy.noWatermarks(), "kafka-source")
                .map(JSON::parseObject);

        //TODO 3.按照Uid进行分组去重
        SingleOutputStreamOperator<TradePaymentBean> tradePaymentBeanDS = kafkaDS.keyBy(json -> json.getString("user_id"))
                .flatMap(new RichFlatMapFunction<JSONObject, TradePaymentBean>() {

                    private ValueState<String> lastDtState;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        lastDtState = getRuntimeContext().getState(new ValueStateDescriptor<String>("last-dt", String.class));
                    }

                    @Override
                    public void flatMap(JSONObject value, Collector<TradePaymentBean> out) throws Exception {

                        //取出相关信息
                        String lastDt = lastDtState.value();
                        Long ts = value.getLong("callback_time");
                        System.out.println("callback_time:" + value.getString("callback_time"));
                        String curDt = value.getString("callback_time").split(" ")[0];

                        long curDtCt = 0L;
                        long newCt = 0L;

                        if (lastDt == null) {
                            curDtCt = 1L;
                            newCt = 1L;
                            lastDtState.update(curDt);
                        } else if (!lastDt.equals(curDt)) {
                            curDtCt = 1L;
                            lastDtState.update(curDt);
                        }

                        if (curDtCt == 1L) {
                            out.collect(new TradePaymentBean("", "",
                                    curDt, curDtCt, newCt, ts));
                        }
                    }
                });

        //TODO 4.开窗聚合
        SingleOutputStreamOperator<TradePaymentBean> reduceDS = tradePaymentBeanDS.assignTimestampsAndWatermarks(WatermarkStrategy.<TradePaymentBean>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<TradePaymentBean>() {
                    @Override
                    public long extractTimestamp(TradePaymentBean element, long recordTimestamp) {
                        return element.getTs();
                    }
                })).windowAll(TumblingEventTimeWindows.of(Time.seconds(10)))
                .reduce(new ReduceFunction<TradePaymentBean>() {
                    @Override
                    public TradePaymentBean reduce(TradePaymentBean value1, TradePaymentBean value2) throws Exception {
                        value1.setPaymentSucNewUserCt(value1.getPaymentSucNewUserCt() + value2.getPaymentSucNewUserCt());
                        value1.setPaymentSucUniqueUserCt(value1.getPaymentSucUniqueUserCt() + value2.getPaymentSucUniqueUserCt());
                        return value1;
                    }
                }, new AllWindowFunction<TradePaymentBean, TradePaymentBean, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow window, Iterable<TradePaymentBean> input, Collector<TradePaymentBean> out) throws Exception {
                        TradePaymentBean next = input.iterator().next();

                        next.setEdt(DateFormatUtil.toYmdHms(window.getEnd()));
                        next.setStt(DateFormatUtil.toYmdHms(window.getStart()));

                        out.collect(next);
                    }
                });

        //TODO 5.写出数据
        reduceDS.print(">>>>>");
        reduceDS.map(bean -> {
                    SerializeConfig config = new SerializeConfig();
                    config.propertyNamingStrategy = PropertyNamingStrategy.SnakeCase;  // 转成json的时候, 属性名使用下划线
                    return JSON.toJSONString(bean, config);
                })
                .sinkTo(DorisUtil.getDorisSink("gmall_230201.dws_trade_payment_suc_window"));

        //TODO 6.启动任务
        env.execute("Dws08_TradePaymentSucWindow");

    }

}
