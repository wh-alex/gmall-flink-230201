package com.atguigu.app.dws;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.atguigu.app.func.DimInfoRichMapFunction;
import com.atguigu.bean.TradeProvinceOrderBean;
import com.atguigu.common.Constant;
import com.atguigu.utils.DateFormatUtil;
import com.atguigu.utils.DorisUtil;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.state.StateTtlConfig;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;
import java.util.HashSet;

//数据流：web/app -> 业务数据库(MySQL) -> Maxwell -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> Doris(DWS)
//程  序：Mock -> 业务数据库(MySQL) -> Maxwell -> Kafka(ZK) -> DwdTradeOrderDetail -> Kafka(ZK) -> Dws10_TradeProvinceOrderWindow(HDFS HBase Redis) -> Doris
public class Dws10_TradeProvinceOrderWindow {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //CheckPoint
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        env.enableCheckpointing(5000L);
        env.setStateBackend(new HashMapStateBackend());
        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dws/keyword");
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
        checkpointConfig.setCheckpointTimeout(10000L);

        //固定延迟重启
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3, 1000L));

        //TODO 2.读取Kafka DWD层 下单明细主题数据
        DataStreamSource<String> kafkaDS = env.fromSource(KafkaUtil.getKafkaSource(Constant.TOPIC_DWD_TRADE_ORDER_DETAIL, "province_order_window_230201"), WatermarkStrategy.noWatermarks(), "kafka-source");

        //TODO 3.过滤Null值并转换为JSON对象
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                if (value != null) {
                    out.collect(JSONObject.parseObject(value));
                }
            }
        });

        //TODO 4.按照order_detail_id进行分组,去重  left join产生的  JavaBean
        SingleOutputStreamOperator<TradeProvinceOrderBean> tradeProvinceOrderDS = jsonObjDS.keyBy(json -> json.getString("id"))
                .flatMap(new RichFlatMapFunction<JSONObject, TradeProvinceOrderBean>() {

                    private ValueState<String> state;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        StateTtlConfig ttlConfig = new StateTtlConfig.Builder(Time.seconds(5))
                                .setUpdateType(StateTtlConfig.UpdateType.OnReadAndWrite)
                                .build();
                        ValueStateDescriptor<String> stateDescriptor = new ValueStateDescriptor<>("state", String.class);
                        stateDescriptor.enableTimeToLive(ttlConfig);

                        state = getRuntimeContext().getState(stateDescriptor);
                    }

                    @Override
                    public void flatMap(JSONObject value, Collector<TradeProvinceOrderBean> out) throws Exception {
                        //取出状态
                        String exist = state.value();
                        if (exist == null) {
                            state.update("1");
                            HashSet<String> orderIds = new HashSet<>();
                            orderIds.add(value.getString("order_id"));
                            out.collect(TradeProvinceOrderBean.builder()
                                    .provinceId(value.getString("province_id"))
                                    .orderIds(orderIds)
                                    .curDate(value.getString("create_time").split(" ")[0])
                                    .orderAmount(value.getBigDecimal("split_total_amount"))
                                    .ts(value.getLong("create_time"))
                                    .build());
                        }
                    }
                });

        //TODO 5.提取时间戳,分组、开窗、聚合
        SingleOutputStreamOperator<TradeProvinceOrderBean> reduceDS = tradeProvinceOrderDS
                .assignTimestampsAndWatermarks(WatermarkStrategy.<TradeProvinceOrderBean>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<TradeProvinceOrderBean>() {
                    @Override
                    public long extractTimestamp(TradeProvinceOrderBean element, long recordTimestamp) {
                        return element.getTs();
                    }
                }))
                .keyBy(TradeProvinceOrderBean::getProvinceId)
                .window(TumblingEventTimeWindows.of(org.apache.flink.streaming.api.windowing.time.Time.seconds(10)))
                .reduce(new ReduceFunction<TradeProvinceOrderBean>() {
                    @Override
                    public TradeProvinceOrderBean reduce(TradeProvinceOrderBean value1, TradeProvinceOrderBean value2) throws Exception {
                        value1.setOrderAmount(value1.getOrderAmount().add(value2.getOrderAmount()));
                        value1.getOrderIds().addAll(value2.getOrderIds());
                        return value1;
                    }
                }, new WindowFunction<TradeProvinceOrderBean, TradeProvinceOrderBean, String, TimeWindow>() {
                    @Override
                    public void apply(String key, TimeWindow window, Iterable<TradeProvinceOrderBean> input, Collector<TradeProvinceOrderBean> out) throws Exception {

                        TradeProvinceOrderBean next = input.iterator().next();

                        next.setStt(DateFormatUtil.toYmdHms(window.getStart()));
                        next.setEdt(DateFormatUtil.toYmdHms(window.getEnd()));
                        next.setOrderCount((long) next.getOrderIds().size());

                        out.collect(next);
                    }
                });

        reduceDS.print("reduceDS>>>>");

        //TODO 6.关联维表补充省份信息
        SingleOutputStreamOperator<TradeProvinceOrderBean> resultDS = reduceDS.map(new DimInfoRichMapFunction<TradeProvinceOrderBean>("dim_base_province") {
            @Override
            public String getRowKey(TradeProvinceOrderBean value) {
                return value.getProvinceId();
            }

            @Override
            public void join(TradeProvinceOrderBean value, JSONObject dimInfo) {
                value.setProvinceName(dimInfo.getString("name"));
            }
        });

        //TODO 7.将数据写出到Doris
        resultDS.print("resultDS>>>>>>");
        resultDS.map(bean -> {
                    SerializeConfig config = new SerializeConfig();
                    config.propertyNamingStrategy = PropertyNamingStrategy.SnakeCase;
                    return JSON.toJSONString(bean, config);
                })
                .sinkTo(DorisUtil.getDorisSink("gmall_230201.dws_trade_province_order_window"));

        //TODO 8.启动
        env.execute("Dws10_TradeProvinceOrderWindow");

    }

}
