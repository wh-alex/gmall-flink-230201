package com.atguigu.app.dws;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.PropertyNamingStrategy;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.atguigu.bean.TradeOrderBean;
import com.atguigu.common.Constant;
import com.atguigu.utils.DateFormatUtil;
import com.atguigu.utils.DorisUtil;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.hashmap.HashMapStateBackend;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.time.Duration;

//数据流：web/app -> 业务数据库(MySQL) -> Maxwell -> Kafka(ODS) -> FlinkApp -> Kafka(DWD) -> FlinkApp -> Doris(DWS)
//程  序：Mock -> 业务数据库(MySQL) -> Maxwell -> Kafka(ZK) -> DwdTradeOrderDetail -> Kafka(ZK) -> Dws07_TradeOrderWindow(CK HDFS) -> Doris
public class Dws07_TradeOrderWindow {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //CheckPoint
        System.setProperty("HADOOP_USER_NAME", "atguigu");
        env.enableCheckpointing(5000L);
        env.setStateBackend(new HashMapStateBackend());
        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dws/keyword");
        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
        checkpointConfig.setCheckpointTimeout(10000L);

        //TODO 2.读取Kafka DWD层下单明细主题数据
        DataStreamSource<String> kafkaDS = env.fromSource(KafkaUtil.getKafkaSource(Constant.TOPIC_DWD_TRADE_ORDER_DETAIL, "order_window_230201"), WatermarkStrategy.noWatermarks(), "kafka-source");

        //TODO 3.转化为JSON,同时过滤null值数据
        SingleOutputStreamOperator<JSONObject> jsonObjDS = kafkaDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                if (value != null) {
                    out.collect(JSON.parseObject(value));
                }
            }
        });

        //TODO 4.按照Uid进行分组,去重数据
        SingleOutputStreamOperator<TradeOrderBean> tradeOrderBeanDS = jsonObjDS.keyBy(json -> json.getString("user_id"))
                .flatMap(new RichFlatMapFunction<JSONObject, TradeOrderBean>() {

                    private ValueState<String> lastDtState;

                    @Override
                    public void open(Configuration parameters) throws Exception {
                        lastDtState = getRuntimeContext().getState(new ValueStateDescriptor<String>("last-dt", String.class));
                    }

                    @Override
                    public void flatMap(JSONObject value, Collector<TradeOrderBean> out) throws Exception {

                        //取出相关数据
                        String lastDt = lastDtState.value();
                        Long ts = value.getLong("create_time");
                        String curDt = value.getString("create_time").split(" ")[0];

                        long curDtCt = 0L;
                        long newCt = 0L;

                        if (lastDt == null) {
                            curDtCt = 1L;
                            newCt = 1L;
                            lastDtState.update(curDt);
                        } else if (!lastDt.equals(curDt)) {
                            curDtCt = 1L;
                            lastDtState.update(curDt);
                        }

                        if (curDtCt == 1L) {
                            out.collect(new TradeOrderBean("", "",
                                    curDt, curDtCt, newCt, ts));
                        }
                    }
                });

        //TODO 5.提取时间戳,开窗聚合
        SingleOutputStreamOperator<TradeOrderBean> reduceDS = tradeOrderBeanDS.assignTimestampsAndWatermarks(WatermarkStrategy.<TradeOrderBean>forBoundedOutOfOrderness(Duration.ofSeconds(2)).withTimestampAssigner(new SerializableTimestampAssigner<TradeOrderBean>() {
                    @Override
                    public long extractTimestamp(TradeOrderBean element, long recordTimestamp) {
                        return element.getTs();
                    }
                })).windowAll(TumblingEventTimeWindows.of(Time.seconds(10)))
                .reduce(new ReduceFunction<TradeOrderBean>() {
                    @Override
                    public TradeOrderBean reduce(TradeOrderBean value1, TradeOrderBean value2) throws Exception {
                        value1.setOrderUniqueUserCount(value1.getOrderUniqueUserCount() + value2.getOrderUniqueUserCount());
                        value1.setOrderNewUserCount(value1.getOrderNewUserCount() + value2.getOrderNewUserCount());
                        return value1;
                    }
                }, new AllWindowFunction<TradeOrderBean, TradeOrderBean, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow window, Iterable<TradeOrderBean> values, Collector<TradeOrderBean> out) throws Exception {
                        TradeOrderBean next = values.iterator().next();

                        next.setEdt(DateFormatUtil.toYmdHms(window.getEnd()));
                        next.setStt(DateFormatUtil.toYmdHms(window.getStart()));

                        out.collect(next);
                    }
                });

        //TODO 6.写出数据
        reduceDS.print(">>>>>");
        reduceDS.map(bean -> {
                    SerializeConfig config = new SerializeConfig();
                    config.propertyNamingStrategy = PropertyNamingStrategy.SnakeCase;  // 转成json的时候, 属性名使用下划线
                    return JSON.toJSONString(bean, config);
                })
                .sinkTo(DorisUtil.getDorisSink("gmall_230201.dws_trade_order_window"));

        //TODO 7.启动任务
        env.execute("Dws07_TradeOrderWindow");

    }

}
