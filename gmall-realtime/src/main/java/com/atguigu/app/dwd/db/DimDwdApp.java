package com.atguigu.app.dwd.db;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.app.func.CreateTableRichMapFunction;
import com.atguigu.app.func.DimDwdTableProcessFunction;
import com.atguigu.app.func.DimSinkFunction;
import com.atguigu.bean.TableProcess;
import com.atguigu.common.Constant;
import com.atguigu.utils.KafkaUtil;
import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import com.ververica.cdc.connectors.mysql.table.StartupOptions;
import com.ververica.cdc.debezium.JsonDebeziumDeserializationSchema;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.streaming.api.datastream.*;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.apache.kafka.clients.producer.ProducerRecord;

import javax.annotation.Nullable;

public class DimDwdApp {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //CheckPoint
//        env.enableCheckpointing(5000L);
//        env.setStateBackend(new HashMapStateBackend());
//        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
//        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dim");
//        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
//        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
//        checkpointConfig.setCheckpointTimeout(10000L);

        //TODO 2.读取Kafka topic_db主题数据
        DataStreamSource<String> sourceDS = env.fromSource(KafkaUtil.getKafkaSource(Constant.TOPIC_ODS_DB, "dim_dwd_app"),
                WatermarkStrategy.noWatermarks(),
                "kafka-source");

        //TODO 3.将数据转换为JSON对象并过滤  主流
        SingleOutputStreamOperator<JSONObject> jsonObjDS = sourceDS.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                try {
                    JSONObject jsonObject = JSONObject.parseObject(value);
                    out.collect(jsonObject);
                } catch (JSONException e) {
                    System.out.println("脏数据：" + value);
                }
            }
        });

        //TODO 4.使用FlinkCDC读取配置信息表并转换为TableProcess对象
        MySqlSource<String> mySqlSource = MySqlSource.<String>builder()
                .hostname(Constant.MYSQL_HOST)
                .port(Constant.MYSQL_PORT)
                .username("root")
                .password("000000")
                .databaseList("gmall-230201-config")
                .tableList("gmall-230201-config.table_process")
                .deserializer(new JsonDebeziumDeserializationSchema())
                .startupOptions(StartupOptions.initial())
                .build();
        DataStreamSource<String> mysqlDS = env.fromSource(mySqlSource, WatermarkStrategy.noWatermarks(), "mysql-source");
        SingleOutputStreamOperator<TableProcess> tableProcessDS = mysqlDS.map(new MapFunction<String, TableProcess>() {
            //value:{"before":{},"after":{},"op":"crud"}
            @Override
            public TableProcess map(String value) throws Exception {
                JSONObject jsonObject = JSONObject.parseObject(value);
                //获取操作类型
                String op = jsonObject.getString("op");

                TableProcess tableProcess = null;
                if ("d".equals(op)) {
                    tableProcess = JSONObject.parseObject(jsonObject.getString("before"), TableProcess.class);
                } else {
                    tableProcess = JSONObject.parseObject(jsonObject.getString("after"), TableProcess.class);
                }

                //将操作类型写入TableProcess对象
                tableProcess.setOp(op);

                return tableProcess;
            }
        });

        //TODO 5.如果为DIM数据,那么建表
        SingleOutputStreamOperator<TableProcess> createTableProcessDS = tableProcessDS.map(new CreateTableRichMapFunction());

        //TODO 6.将配置信息流转换为广播流
        MapStateDescriptor<String, TableProcess> stateDescriptor = new MapStateDescriptor<>("map-state", String.class, TableProcess.class);
        BroadcastStream<TableProcess> broadcastDS = createTableProcessDS.broadcast(stateDescriptor);

        //TODO 7.连接主流与广播流
        BroadcastConnectedStream<JSONObject, TableProcess> connectDS = jsonObjDS.connect(broadcastDS);

        //TODO 8.根据广播流处理主流数据  主流数据(Kafka) 侧输出流数据(HBase)
        OutputTag<JSONObject> hbaseOutputTag = new OutputTag<JSONObject>("hbase") {
        };
        SingleOutputStreamOperator<JSONObject> kafkaDS = connectDS.process(new DimDwdTableProcessFunction(hbaseOutputTag, stateDescriptor));

        //TODO 9.获取HBase数据
        SideOutputDataStream<JSONObject> hbaseDS = kafkaDS.getSideOutput(hbaseOutputTag);

        //TODO 10.将Kafka数据写入对应的Kafka主题,将HBase数据写入对应的HBase表
        kafkaDS.print("kafkaDS>>>>>");
        hbaseDS.print("hbaseDS>>>>>");

        kafkaDS.sinkTo(KafkaUtil.getKafkaSink(new KafkaRecordSerializationSchema<JSONObject>() {
            @Nullable
            @Override
            public ProducerRecord<byte[], byte[]> serialize(JSONObject element, KafkaSinkContext context, Long timestamp) {
                return new ProducerRecord<>(element.getString("sinkTable"),
                        element.getString("data").getBytes());
            }
        }));

        hbaseDS.addSink(new DimSinkFunction());

        //TODO 11.启动任务
        env.execute();

    }
}
