package com.atguigu.app.dwd.db;

import com.atguigu.common.Constant;
import com.atguigu.utils.HBaseUtil;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

//数据流：web/app -> 业务数据库(MySQL) -> Maxwell -> Kafka(ODS) -> FlinkApp(HBase) -> Kafka(DWD)
//程  序：Mock -> 业务数据库(MySQL) -> Maxwell -> Kafka(ZK) -> DwdInteractionCommentInfo(HBase ZK HDFS) -> Kafka(ZK)
public class DwdInteractionCommentInfo {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //CheckPoint
//        env.enableCheckpointing(5000L);
//        env.setStateBackend(new HashMapStateBackend());
//        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
//        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dim");
//        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
//        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
//        checkpointConfig.setCheckpointTimeout(10000L);

        //TODO 2.读取Kafka topic_db主题数据
        tableEnv.executeSql(KafkaUtil.getKafkaTopicDB("dwd_comment_info"));

//        tableEnv.sqlQuery("select * from topic_db")
//                .execute()
//                .print();

        //TODO 3.过滤出评价表数据
        Table table = tableEnv.sqlQuery("" +
                "select\n" +
                "    `data`['id'] id,\n" +
                "    `data`['user_id'] user_id,\n" +
                "    `data`['nick_name'] nick_name,\n" +
                "    `data`['head_img'] head_img,\n" +
                "    `data`['sku_id'] sku_id,\n" +
                "    `data`['spu_id'] spu_id,\n" +
                "    `data`['order_id'] order_id,\n" +
                "    `data`['appraise'] appraise,\n" +
                "    `data`['comment_txt'] comment_txt,\n" +
                "    `data`['create_time'] create_time,\n" +
                "    `data`['operate_time'] operate_time,\n" +
                "    `pt`\n" +
                "from topic_db\n" +
                "where `database` = 'gmall-230201-flink'\n" +
                "and `table` = 'comment_info' \n" +
                "and `type` = 'insert'");
        tableEnv.createTemporaryView("comment_info", table);

        //TODO 4.读取HBase base_dic表创建LookUp表
        tableEnv.executeSql(HBaseUtil.getHBaseDicDDL());

        //打印测试
        //tableEnv.sqlQuery("select *,info.sku_id sku_id from base_dic").execute().print();

        //TODO 5.LookUpJoin
        Table resultTable = tableEnv.sqlQuery("" +
                "select\n" +
                "    comment_info.`id`,\n" +
                "    comment_info.`user_id`,\n" +
                "    comment_info.`nick_name`,\n" +
                "    comment_info.`head_img`,\n" +
                "    comment_info.`sku_id`,\n" +
                "    comment_info.`spu_id`,\n" +
                "    comment_info.`order_id`,\n" +
                "    comment_info.`appraise`,\n" +
                "    base_dic.`dic_name` appraise_name,\n" +
                "    comment_info.`comment_txt`,\n" +
                "    comment_info.`create_time`,\n" +
                "    comment_info.`operate_time`\n" +
                "from\n" +
                "comment_info join base_dic FOR SYSTEM_TIME AS OF comment_info.`pt`\n" +
                "on comment_info.`appraise` = base_dic.`rowkey`");
        tableEnv.createTemporaryView("result_table", resultTable);

        //打印测试
        //tableEnv.toDataStream(resultTable).print();

        //TODO 6.将数据写出到Kafka
        tableEnv.executeSql("" +
                "create table dwd_comment_info(\n" +
                "    `id` string,\n" +
                "    `user_id` string,\n" +
                "    `nick_name` string,\n" +
                "    `head_img` string,\n" +
                "    `sku_id` string,\n" +
                "    `spu_id` string,\n" +
                "    `order_id` string,\n" +
                "    `appraise` string,\n" +
                "    `appraise_name` string,\n" +
                "    `comment_txt` string,\n" +
                "    `create_time` string,\n" +
                "    `operate_time` string\n" +
                ")" + KafkaUtil.getKafkaSinkDDL(Constant.TOPIC_DWD_INTERACTION_COMMENT_INFO));
        tableEnv.executeSql("insert into dwd_comment_info select * from result_table");

        //env.execute();
    }

}
