package com.atguigu.app.dwd.db;

import com.atguigu.common.Constant;
import com.atguigu.utils.KafkaUtil;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

//数据流：web/app -> 业务数据库(MySQL) -> Maxwell -> Kafka(ODS) -> FlinkApp -> Kafka(DWD)
//程  序：Mock -> 业务数据库(MySQL) -> Maxwell -> Kafka(ZK) -> DwdTradeCartAdd -> Kafka(ZK)
public class DwdTradeCartAdd {

    public static void main(String[] args) {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //CheckPoint
//        env.enableCheckpointing(5000L);
//        env.setStateBackend(new HashMapStateBackend());
//        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
//        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dim");
//        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
//        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
//        checkpointConfig.setCheckpointTimeout(10000L);

        //TODO 2.读取Kafka topic_db 主题数据
        tableEnv.executeSql(KafkaUtil.getKafkaTopicDB("dwd_trade_cart_add"));

        //TODO 3.过滤加购数据
        Table resultTable = tableEnv.sqlQuery("" +
                "select\n" +
                "    `data`['id'] id,\n" +
                "    `data`['user_id'] user_id,\n" +
                "    `data`['sku_id'] sku_id,\n" +
                "    `data`['cart_price'] cart_price,\n" +
                "    if(`type` = 'insert',`data`['sku_num'],cast(cast(`data`['sku_num'] as int)-cast(`old`['sku_num'] as int) as string)) sku_num,\n" +
                "    `data`['img_url'] img_url,\n" +
                "    `data`['sku_name'] sku_name,\n" +
                "    `data`['is_checked'] is_checked,\n" +
                "    `data`['create_time'] create_time,\n" +
                "    `data`['operate_time'] operate_time,\n" +
                "    `data`['is_ordered'] is_ordered,\n" +
                "    `data`['order_time'] order_time,\n" +
                "    `data`['source_type'] source_type,\n" +
                "    `data`['source_id'] source_id\n" +
                "from topic_db\n" +
                "where `database` = 'gmall-230201-flink'\n" +
                "and `table` = 'cart_info' \n" +
                "and (`type` = 'insert' or (`type` = 'update' and `old`['sku_num'] is not null and cast(`old`['sku_num'] as int) < cast(`data`['sku_num'] as int)))");
        tableEnv.createTemporaryView("result_table", resultTable);

        //打印测试
//        tableEnv.sqlQuery("select * from result_table")
//                .execute().print();

        //TODO 4.写出到Kafka
        tableEnv.executeSql("" +
                "create table dwd_cart_info(\n" +
                "    `id` string,\n" +
                "    `user_id` string,\n" +
                "    `sku_id` string,\n" +
                "    `cart_price` string,\n" +
                "    `sku_num` string,\n" +
                "    `img_url` string,\n" +
                "    `sku_name` string,\n" +
                "    `is_checked` string,\n" +
                "    `create_time` string,\n" +
                "    `operate_time` string,\n" +
                "    `is_ordered` string,\n" +
                "    `order_time` string,\n" +
                "    `source_type` string,\n" +
                "    `source_id` string\n" +
                ")" + KafkaUtil.getKafkaSinkDDL(Constant.TOPIC_DWD_TRADE_CART_ADD));
        tableEnv.executeSql("insert into dwd_cart_info select * from result_table");


    }

}
