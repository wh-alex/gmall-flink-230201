package com.atguigu.app.dim;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.app.func.CreateTableRichMapFunction;
import com.atguigu.app.func.DimSinkFunction;
import com.atguigu.app.func.DimTableProcessFunction;
import com.atguigu.bean.TableProcess;
import com.atguigu.common.Constant;
import com.atguigu.utils.KafkaUtil;
import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import com.ververica.cdc.connectors.mysql.table.StartupOptions;
import com.ververica.cdc.debezium.JsonDebeziumDeserializationSchema;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.streaming.api.datastream.BroadcastConnectedStream;
import org.apache.flink.streaming.api.datastream.BroadcastStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

//数据流:web/app -> 业务服务器(MySQL) -> Maxwell -> Kafka(ODS) -> FlinkApp(FlinkCDC) ->  HBase(DIM)
//程  序:Mock -> Mysql -> Maxwell -> Kafka(ZK) -> DimApp -> HBase(ZK,HDFS)
public class DimApp {

    public static void main(String[] args) throws Exception {

        //TODO 1.获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //CheckPoint
//        env.enableCheckpointing(5000L);
//        env.setStateBackend(new HashMapStateBackend());
//        CheckpointConfig checkpointConfig = env.getCheckpointConfig();
//        checkpointConfig.setCheckpointStorage("hdfs://hadoop102:8020/gmall/dim");
//        checkpointConfig.setExternalizedCheckpointCleanup(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
//        checkpointConfig.setMinPauseBetweenCheckpoints(2000L);
//        checkpointConfig.setCheckpointTimeout(10000L);

        //TODO 2.读取Kafka topic_db主题数据,并将数据转换为JSON对象(主流)
        SingleOutputStreamOperator<JSONObject> jsonObjDS = env.fromSource(KafkaUtil.getKafkaSource(Constant.TOPIC_ODS_DB, "dim_app_230201"),
                        WatermarkStrategy.noWatermarks(),
                        "kafka-source")
                .flatMap(new FlatMapFunction<String, JSONObject>() {
                    @Override
                    public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                        try {
                            JSONObject jsonObject = JSON.parseObject(value);
                            out.collect(jsonObject);
                        } catch (Exception e) {
                            System.out.println("脏数据为：" + value);
                        }
                    }
                });

        jsonObjDS.print("jsonObjDS>>>>>>");

        //TODO 3.使用FlinkCDC读取MySQL中配置信息表，并将数据转换为JavaBean对象 配置流
        MySqlSource<String> mySqlSource = MySqlSource.<String>builder()
                .hostname(Constant.MYSQL_HOST)
                .port(Constant.MYSQL_PORT)
                .username("root")
                .password("000000")
                .databaseList("gmall-230201-config")
                .tableList("gmall-230201-config.table_process")
                .deserializer(new JsonDebeziumDeserializationSchema())
                .startupOptions(StartupOptions.initial())
                .build();
        SingleOutputStreamOperator<TableProcess> tableProcessDS = env.fromSource(mySqlSource, WatermarkStrategy.noWatermarks(), "mysql-source")
                .map(new MapFunction<String, TableProcess>() {
                    @Override
                    //Value:{"before":null,"after":{"source_table":"base_category3","sink_table":"dim_base_category3","sink_columns":"id,name,category2_id","sink_pk":"id","sink_extend":null},"source":{"version":"1.5.4.Final","connector":"mysql","name":"mysql_binlog_source","ts_ms":1669162876406,"snapshot":"false","db":"gmall-220623-config","sequence":null,"table":"table_process","server_id":0,"gtid":null,"file":"","pos":0,"row":0,"thread":null,"query":null},"op":"r","ts_ms":1669162876406,"transaction":null}
                    public TableProcess map(String value) throws Exception {
                        JSONObject jsonObject = JSON.parseObject(value);
                        //获取操作类型
                        String op = jsonObject.getString("op");
                        TableProcess tableProcess = null;
                        if ("d".equals(op)) {
                            tableProcess = JSON.parseObject(jsonObject.getString("before"), TableProcess.class);
                        } else {
                            tableProcess = JSON.parseObject(jsonObject.getString("after"), TableProcess.class);
                        }
                        //补充op信息
                        tableProcess.setOp(op);
                        return tableProcess;
                    }
                }).filter(tableProcess -> "dim".equals(tableProcess.getSinkType()));
        SingleOutputStreamOperator<TableProcess> tableProcessWithCreateTableDS = tableProcessDS.map(new CreateTableRichMapFunction());

        //TODO 4.将配置流处理成广播流  广播流
        MapStateDescriptor<String, TableProcess> stateDescriptor = new MapStateDescriptor<>("map-state", String.class, TableProcess.class);
        BroadcastStream<TableProcess> broadcastStream = tableProcessWithCreateTableDS.broadcast(stateDescriptor);

        //TODO 5.连接主流和广播流
        BroadcastConnectedStream<JSONObject, TableProcess> connectedStream = jsonObjDS.connect(broadcastStream);

        //TODO 6.根据广播状态处理主流数据
        SingleOutputStreamOperator<JSONObject> dimDS = connectedStream.process(new DimTableProcessFunction(stateDescriptor));

        //打印测试
        dimDS.print("HBase>>>>");

        //TODO 7.将维表数据写出到HBase
        dimDS.addSink(new DimSinkFunction());

        //TODO 8.启动任务
        env.execute("DimApp");

    }

}
