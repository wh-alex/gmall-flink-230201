package com.atguigu.app.func;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TableProcess;
import com.atguigu.common.Constant;
import com.atguigu.utils.JdbcUtil;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.*;

public class DimDwdTableProcessFunction extends BroadcastProcessFunction<JSONObject, TableProcess, JSONObject> {

    private OutputTag<JSONObject> hbaseOutputTag;
    private MapStateDescriptor<String, TableProcess> stateDescriptor;

    private HashMap<String, TableProcess> tableProcessHashMap;

    public DimDwdTableProcessFunction(OutputTag<JSONObject> hbaseOutputTag, MapStateDescriptor<String, TableProcess> stateDescriptor) {
        this.hbaseOutputTag = hbaseOutputTag;
        this.stateDescriptor = stateDescriptor;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        //初始化HashMap
        tableProcessHashMap = new HashMap<>();

        //预加载配置信息
        //加载驱动
        Class.forName(Constant.MYSQL_DRIVER);
        //获取连接
        Connection connection = DriverManager.getConnection(Constant.MYSQL_URL, "root", "000000");
        //查询
        List<TableProcess> list = JdbcUtil.queryList(connection,
                "select * from table_process",
                TableProcess.class,
                true);

        //关闭连接
        connection.close();

        //遍历list,将数据放入Map中
        for (TableProcess tableProcess : list) {
            String sinkType = tableProcess.getSinkType();
            if ("dim".equals(sinkType)) {
                tableProcessHashMap.put(tableProcess.getSourceTable(), tableProcess);
            } else {
                tableProcessHashMap.put(tableProcess.getSourceTable() + "-" + tableProcess.getSourceType(), tableProcess);
            }
        }
    }

    //value:{"database":"gmall-220623-flink","table":"comment_info","type":"insert","ts":1669162958,"xid":1111,"xoffset":13941,"data":{"id":1595211185799847960,"user_id":119,"nick_name":null,"head_img":null,"sku_id":31,"spu_id":10,"order_id":987,"appraise":"1204","comment_txt":"评论内容：48384811984748167197482849234338563286217912223261","create_time":"2022-08-02 08:22:38","operate_time":null}}
    @Override
    public void processElement(JSONObject value, BroadcastProcessFunction<JSONObject, TableProcess, JSONObject>.ReadOnlyContext ctx, Collector<JSONObject> out) throws Exception {

        //获取广播流数据
        ReadOnlyBroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(stateDescriptor);
        String sourceTable = value.getString("table");
        String sourceType = value.getString("type");
        String dwdKey = sourceTable + "-" + sourceType;

        TableProcess dimTableProcess = broadcastState.get(sourceTable);
        TableProcess dimTableProcessMap = tableProcessHashMap.get(sourceTable);

        TableProcess dwdTableProcess = broadcastState.get(dwdKey);
        TableProcess dwdTableProcessMap = tableProcessHashMap.get(dwdKey);

        //行过滤
        if ((dimTableProcess != null || dimTableProcessMap != null) && !("bootstrap-start".equals(sourceType) || "bootstrap-complete".equals(sourceType))) {
            //DIM 列过滤
            if (dimTableProcess == null) {
                dimTableProcess = dimTableProcessMap;
            }
            filterColumns(value.getJSONObject("data"), dimTableProcess.getSinkColumns());

            //补充字段
            value.put("sinkTable", dimTableProcess.getSinkTable());
            value.put("rowKey", dimTableProcess.getSinkRowKey());
            value.put("extend", dimTableProcess.getSinkExtend());
            value.put("cf", dimTableProcess.getSinkFamily());

            //通过侧输出流写入HBase流
            ctx.output(hbaseOutputTag, value);
        } else {
            System.out.println("没有对应的维度表配置信息：" + sourceTable + ",或者类型不对：" + sourceType);
        }

        //行过滤
        if (dwdTableProcess != null || dwdTableProcessMap != null) {

            //DWD 列过滤
            if (dwdTableProcess == null) {
                dwdTableProcess = dwdTableProcessMap;
            }
            filterColumns(value.getJSONObject("data"), dwdTableProcess.getSinkColumns());

            //补充字段
            value.put("sinkTable", dwdTableProcess.getSinkTable());

            //将数据输出到主流 Kafka
            out.collect(value);
        } else {
            System.out.println("没有对应的事实表配置信息：" + dwdKey);
        }
    }

    //列过滤
    private void filterColumns(JSONObject data, String sinkColumns) {
        String[] split = sinkColumns.split(",");
        List<String> columnList = Arrays.asList(split);

        //遍历data,判断每个列是否存在于columnList中
//        Set<Map.Entry<String, Object>> entries = data.entrySet();
//        Iterator<Map.Entry<String, Object>> iterator = entries.iterator();
//        while (iterator.hasNext()) {
//            Map.Entry<String, Object> next = iterator.next();
//            if (!columnList.contains(next.getKey())) {
//                iterator.remove();
//            }
//        }

        Set<Map.Entry<String, Object>> entries = data.entrySet();
        entries.removeIf(next -> !columnList.contains(next.getKey()));
    }

    @Override
    public void processBroadcastElement(TableProcess value, BroadcastProcessFunction<JSONObject, TableProcess, JSONObject>.Context ctx, Collector<JSONObject> out) throws Exception {

        //获取广播状态
        BroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(stateDescriptor);

        //获取操作类型
        String op = value.getOp();

        //获取输出类型 dim/dwd
        String sinkType = value.getSinkType();
        String key = null;
        if ("dim".equals(sinkType)) {
            key = value.getSourceTable();
        } else {
            key = value.getSourceTable() + "-" + value.getSourceType();
        }

        if ("d".equals(op)) {
            broadcastState.remove(key);
            tableProcessHashMap.remove(key);
        } else {
            broadcastState.put(key, value);
        }
    }
}
