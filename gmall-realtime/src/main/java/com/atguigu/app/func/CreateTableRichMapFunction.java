package com.atguigu.app.func;

import com.atguigu.bean.TableProcess;
import com.atguigu.common.Constant;
import com.atguigu.utils.HBaseUtil;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.hadoop.hbase.client.Connection;

import java.io.IOException;

public class CreateTableRichMapFunction extends RichMapFunction<TableProcess, TableProcess> {

    private Connection connection;

    @Override
    public void open(Configuration parameters) throws Exception {
        connection = HBaseUtil.getConnection();
    }

    @Override
    public TableProcess map(TableProcess value) throws Exception {

        String sinkType = value.getSinkType();
        if ("dim".equals(sinkType)) {
            //获取数据的操作类型
            String op = value.getOp();
            if ("r".equals(op) || "c".equals(op)) {
                createTable(connection, value.getSinkTable(), value.getSinkExtend(), value.getSinkFamily());
            } else if ("u".equals(op)) {
                //先删除表,再创建
                HBaseUtil.deleteTable(connection, Constant.HBASE_NAME_SPACE, value.getSinkTable());
                createTable(connection, value.getSinkTable(), value.getSinkExtend(), value.getSinkFamily());
            } else {
                //删除表
                HBaseUtil.deleteTable(connection, Constant.HBASE_NAME_SPACE, value.getSinkTable());
            }
        }
        return value;
    }

    private void createTable(Connection connection, String table, String extend, String family) throws IOException {
        byte[][] bytes = null;
        if (extend != null) {
            String[] splitKeys = extend.split(",");
            bytes = new byte[splitKeys.length][];
            for (int i = 0; i < splitKeys.length; i++) {
                String splitKey = splitKeys[i];
                bytes[i] = splitKey.getBytes();
            }
        }

        //直接建表
        HBaseUtil.createTable(connection,
                Constant.HBASE_NAME_SPACE,
                table,
                bytes,
                family);
    }

    @Override
    public void close() throws Exception {
        HBaseUtil.closeConnection(connection);
    }
}
