package com.atguigu.app.func;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.utils.DimUtil;
import com.atguigu.utils.HBaseUtil;
import com.atguigu.utils.JedisUtil;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.hadoop.hbase.client.Connection;
import redis.clients.jedis.Jedis;

public abstract class DimInfoRichMapFunction<T> extends RichMapFunction<T, T> implements DimJoinFunction<T> {

    private String tableName;

    public DimInfoRichMapFunction(String tableName) {
        this.tableName = tableName;
    }

    private Connection connection;
    private Jedis jedis;

    @Override
    public void open(Configuration parameters) throws Exception {
        connection = HBaseUtil.getConnection();
        jedis = JedisUtil.getJedis();
    }

    @Override
    public T map(T value) throws Exception {

        //查询维表
        JSONObject dimInfo = DimUtil.getDimInfo(jedis, connection, tableName, getRowKey(value));

        //将信息补充至Value中
        join(value, dimInfo);

        return value;
    }

    @Override
    public void close() throws Exception {

    }
}
