package com.atguigu.app.func;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TableProcess;
import com.atguigu.common.Constant;
import com.atguigu.utils.JdbcUtil;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.*;

public class DwdTableProcessFunction extends BroadcastProcessFunction<JSONObject, String, JSONObject> {

    private MapStateDescriptor<String, TableProcess> mapStateDescriptor;
    private HashMap<String, TableProcess> tableProcessHashMap;

    public DwdTableProcessFunction(MapStateDescriptor<String, TableProcess> mapStateDescriptor) {
        this.mapStateDescriptor = mapStateDescriptor;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        //初始化HashMap
        tableProcessHashMap = new HashMap<>();

        //预加载配置信息
        //加载驱动
        Class.forName(Constant.MYSQL_DRIVER);
        //获取连接
        Connection connection = DriverManager.getConnection(Constant.MYSQL_URL, "root", "000000");
        //查询
        List<TableProcess> list = JdbcUtil.queryList(connection,
                "select * from table_process where sink_type='dwd'",
                TableProcess.class,
                true);

        //关闭连接
        connection.close();

        //遍历list,将数据放入Map中
        for (TableProcess tableProcess : list) {
            String sourceTable = tableProcess.getSourceTable();
            String sourceType = tableProcess.getSourceType();
            String key = sourceTable + "-" + sourceType;
            tableProcessHashMap.put(key, tableProcess);
        }
    }

    //value:{"before":null,"after":{"source_table":"base_category3","source_type":"insert","sink_table":"dim_base_category3","sink_columns":"id,name,category2_id","sink_pk":"id","sink_extend":null},"source":{"version":"1.5.4.Final","connector":"mysql","name":"mysql_binlog_source","ts_ms":1669162876406,"snapshot":"false","db":"gmall-220623-config","sequence":null,"table":"table_process","server_id":0,"gtid":null,"file":"","pos":0,"row":0,"thread":null,"query":null},"op":"r","ts_ms":1669162876406,"transaction":null}
    @Override
    public void processBroadcastElement(String value, BroadcastProcessFunction<JSONObject, String, JSONObject>.Context ctx, Collector<JSONObject> out) throws Exception {

        //获取广播状态
        BroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(mapStateDescriptor);

        JSONObject jsonObject = JSON.parseObject(value);
        String op = jsonObject.getString("op");

        if ("d".equals(op)) {
            JSONObject before = jsonObject.getJSONObject("before");
            String sourceTable = before.getString("source_table");
            String sourceType = before.getString("source_type");
            String key = sourceTable + "-" + sourceType;
            broadcastState.remove(key);
            tableProcessHashMap.remove(key);
        } else {
            TableProcess tableProcess = JSON.parseObject(jsonObject.getString("after"), TableProcess.class);
            if ("dwd".equals(tableProcess.getSinkType())) {
                String sourceTable = tableProcess.getSourceTable();
                String sourceType = tableProcess.getSourceType();
                String key = sourceTable + "-" + sourceType;
                broadcastState.put(key, tableProcess);
            }
        }
    }

    //value:{"database":"gmall-220623-flink","table":"comment_info","type":"insert","ts":1669162958,"xid":1111,"xoffset":13941,"data":{"id":1595211185799847960,"user_id":119,"nick_name":null,"head_img":null,"sku_id":31,"spu_id":10,"order_id":987,"appraise":"1204","comment_txt":"评论内容：48384811984748167197482849234338563286217912223261","create_time":"2022-08-02 08:22:38","operate_time":null},"sinkTable":"dwd_xxx_xxx"}
    @Override
    public void processElement(JSONObject value, BroadcastProcessFunction<JSONObject, String, JSONObject>.ReadOnlyContext ctx, Collector<JSONObject> out) throws Exception {

        //获取状态
        ReadOnlyBroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(mapStateDescriptor);

        //获取表名和操作类型
        String sourceTable = value.getString("table");
        String sourceType = value.getString("type");
        String key = sourceTable + "-" + sourceType;
        TableProcess tableProcess = broadcastState.get(key);
        TableProcess tableProcessMap = tableProcessHashMap.get(key);

        //判断状态是否有数据
        if (tableProcess != null || tableProcessMap != null) {  //行过滤

            if (tableProcess == null) {
                tableProcess = tableProcessMap;
            }

            //列过滤
            filterColumn(value.getJSONObject("data"), tableProcess.getSinkColumns());
            //补充SinkTable字段(Kafka主题)
            value.put("sinkTable", tableProcess.getSinkTable());
            //将数据写出
            out.collect(value);
        } else {
            System.out.println("没有对应的配置信息：" + key);
        }

    }

    /**
     * @param data        {"id":"1","tm_name":"华为","logo_url":"/xx/yy","ct":"xx-xx","ot":"xx-xx"}
     * @param sinkColumns tm_name
     *                    {"id":"1","tm_name":"华为"}
     */
    private void filterColumn(JSONObject data, String sinkColumns) {

        String[] split = sinkColumns.split(",");
        List<String> columnList = Arrays.asList(split);

        Set<Map.Entry<String, Object>> entrySet = data.entrySet();
        Iterator<Map.Entry<String, Object>> iterator = entrySet.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> next = iterator.next();
            String key = next.getKey();
            if (!columnList.contains(key)) {
                iterator.remove();
            }
        }
    }
}
