package com.atguigu.app.func;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TableProcess;
import com.atguigu.common.Constant;
import com.atguigu.utils.JdbcUtil;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.*;

public class DimTableProcessFunction extends BroadcastProcessFunction<JSONObject, TableProcess, JSONObject> {

    private MapStateDescriptor<String, TableProcess> stateDescriptor;
    private HashMap<String, TableProcess> tableProcessHashMap;

    public DimTableProcessFunction(MapStateDescriptor<String, TableProcess> stateDescriptor) {
        this.stateDescriptor = stateDescriptor;
    }

    @Override
    public void open(Configuration parameters) throws Exception {

        //初始化HashMap
        tableProcessHashMap = new HashMap<>();

        //预加载配置信息
        //加载驱动
        Class.forName(Constant.MYSQL_DRIVER);
        //获取连接
        Connection connection = DriverManager.getConnection(Constant.MYSQL_URL, "root", "000000");
        //查询
        List<TableProcess> list = JdbcUtil.queryList(connection,
                "select * from table_process where sink_type='dim'",
                TableProcess.class,
                true);

        //关闭连接
        connection.close();

        //遍历list,将数据放入Map中
        for (TableProcess tableProcess : list) {
            tableProcessHashMap.put(tableProcess.getSourceTable(), tableProcess);
        }
    }

    @Override
    public void processBroadcastElement(TableProcess value, BroadcastProcessFunction<JSONObject, TableProcess, JSONObject>.Context ctx, Collector<JSONObject> out) throws Exception {
        //将数据存储到状态中
        BroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(stateDescriptor);

        String sourceTable = value.getSourceTable();
        //如果配置信息表内容被删除,则在状态中也删除对应的信息
        if ("d".equals(value.getOp())) {
            broadcastState.remove(sourceTable);
            tableProcessHashMap.remove(sourceTable);
        } else {
            broadcastState.put(sourceTable, value);
        }
    }

    //value:{"database":"gmall-220623-flink","table":"comment_info","type":"insert","ts":1669162958,"xid":1111,"xoffset":13941,"data":{"id":1595211185799847960,"user_id":119,"nick_name":null,"head_img":null,"sku_id":31,"spu_id":10,"order_id":987,"appraise":"1204","comment_txt":"评论内容：48384811984748167197482849234338563286217912223261","create_time":"2022-08-02 08:22:38","operate_time":null}}
    @Override
    public void processElement(JSONObject value, BroadcastProcessFunction<JSONObject, TableProcess, JSONObject>.ReadOnlyContext ctx, Collector<JSONObject> out) throws Exception {

        //System.out.println("processElement>>>>" + value);

        //获取状态
        ReadOnlyBroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(stateDescriptor);

        //取出当前数据中的对应状态
        String table = value.getString("table");
        TableProcess tableProcess = broadcastState.get(table);
        TableProcess tableProcessMap = tableProcessHashMap.get(table);

        //获取当前数据的操作类型
        String type = value.getString("type");

        //过滤数据
        if ((tableProcess != null || tableProcessMap != null) && !"bootstrap-start".equals(type) && !"bootstrap-complete".equals(type)) {

            if (tableProcess == null) {
                tableProcess = tableProcessMap;
            }

            //对列进行过滤
            JSONObject data = value.getJSONObject("data");
            filterColumn(data, tableProcess.getSinkRowKey(), tableProcess.getSinkColumns());

            //将主键以及表名存入数据
            value.put("rowKey", tableProcess.getSinkRowKey());
            value.put("sinkTable", tableProcess.getSinkTable());
            value.put("cf", tableProcess.getSinkFamily());
            String sinkExtend = tableProcess.getSinkExtend();
            if (sinkExtend != null) {
                value.put("extend", sinkExtend);
            }

            out.collect(value);
        } else {
            if (tableProcess == null) {
                System.out.println("没有对应的配置信息！");
            } else {
                System.out.println("数据操作类型不符合:" + type);
            }
        }
    }

    /**
     * @param data        {"id":"1","tm_name":"华为","logo_url":"/xx/yy","ct":"xx-xx","ot":"xx-xx"}
     * @param sinkRowKey  id
     * @param sinkColumns tm_name
     *                    {"id":"1","tm_name":"华为"}
     */
    private void filterColumn(JSONObject data, String sinkRowKey, String sinkColumns) {

        String[] split = sinkColumns.split(",");
        List<String> columnList = Arrays.asList(split);

        Set<Map.Entry<String, Object>> entrySet = data.entrySet();
        Iterator<Map.Entry<String, Object>> iterator = entrySet.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> next = iterator.next();
            String key = next.getKey();
            if (!sinkRowKey.equals(key) && !columnList.contains(key)) {
                iterator.remove();
            }
        }
    }
}
