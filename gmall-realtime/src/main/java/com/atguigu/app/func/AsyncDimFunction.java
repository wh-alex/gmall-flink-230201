package com.atguigu.app.func;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.utils.DimUtil;
import com.atguigu.utils.HBaseUtil;
import com.atguigu.utils.JedisUtil;
import io.lettuce.core.api.StatefulRedisConnection;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;
import org.apache.hadoop.hbase.client.AsyncConnection;

import java.util.Collections;

public abstract class AsyncDimFunction<T> extends RichAsyncFunction<T, T> implements DimJoinFunction<T> {

    private StatefulRedisConnection<String, String> redisAsyncConnection;
    private AsyncConnection hbaseAsyncConnection;

    private String tableName;

    public AsyncDimFunction(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        //初始化连接
        redisAsyncConnection = JedisUtil.getAsyncRedisConnection();
        hbaseAsyncConnection = HBaseUtil.getAsyncConnection();
    }

    @Override
    public void asyncInvoke(T input, ResultFuture<T> resultFuture) throws Exception {

        //查询维度数据
        JSONObject dimInfoByAsync = DimUtil.getDimInfoByAsync(redisAsyncConnection.async(),
                hbaseAsyncConnection,
                tableName,
                getRowKey(input));

        //补充维度信息
        join(input, dimInfoByAsync);

        //输出
        resultFuture.complete(Collections.singletonList(input));
    }

    @Override
    public void timeout(T input, ResultFuture<T> resultFuture) throws Exception {
        System.out.println("TimeOut>>>>>>>>" + input);
    }

    @Override
    public void close() throws Exception {
        hbaseAsyncConnection.close();
        redisAsyncConnection.close();
    }
}
