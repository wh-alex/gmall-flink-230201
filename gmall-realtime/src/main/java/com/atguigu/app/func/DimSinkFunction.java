package com.atguigu.app.func;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.Constant;
import com.atguigu.utils.HBaseUtil;
import com.atguigu.utils.JedisUtil;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.hadoop.hbase.client.Connection;
import redis.clients.jedis.Jedis;

public class DimSinkFunction extends RichSinkFunction<JSONObject> {

    private Connection connection;
    private Jedis jedis;

    @Override
    public void open(Configuration parameters) throws Exception {
        connection = HBaseUtil.getConnection();
        jedis = JedisUtil.getJedis();
    }

    //value:{"database":"gmall-220623-flink","table":"comment_info","type":"insert","ts":1669162958,"xid":1111,"xoffset":13941,"data":{"id":1595211185799847960,"user_id":119,"nick_name":null,"head_img":null,"sku_id":31,"spu_id":10,"order_id":987,"appraise":"1204","comment_txt":"评论内容：48384811984748167197482849234338563286217912223261","create_time":"2022-08-02 08:22:38","operate_time":null},"rowKey":"id","sinkTable":"dim_xxx_xxx","cf":"f1","extend":"a|,b|..."}
    @Override
    public void invoke(JSONObject value, Context context) throws Exception {

        //获取当前数据的操作类型
        String type = value.getString("type");

        //如果为删除操作,则在HBase中将数据也删除
        String sinkTable = value.getString("sinkTable");
        JSONObject data = value.getJSONObject("data");
        String rowKey = data.getString(value.getString("rowKey"));

        //如果是更新操作,则先将数据写出到Redis
        if ("update".equals(type)) {
            String redisKey = "DIM:" + sinkTable + ":" + rowKey;
            jedis.setex(redisKey, 24 * 3600, data.toJSONString());
        } else if ("delete".equals(type)) {
            String redisKey = "DIM:" + sinkTable + ":" + rowKey;
            jedis.del(redisKey);
        }

        //获取扩展字段
        String extend = value.getString("extend");
        if (extend != null) {
            rowKey = HBaseUtil.getRowKey(rowKey, extend);
        }

        if ("delete".equals(type)) {
            HBaseUtil.deleteData(connection,
                    Constant.HBASE_NAME_SPACE,
                    sinkTable,
                    rowKey);
        } else {
            data.remove(value.getString("rowKey"));
            HBaseUtil.putData(connection,
                    Constant.HBASE_NAME_SPACE,
                    sinkTable,
                    rowKey,
                    value.getString("cf"),
                    data);
        }
    }

    @Override
    public void close() throws Exception {
        HBaseUtil.closeConnection(connection);
        jedis.close();
    }
}
