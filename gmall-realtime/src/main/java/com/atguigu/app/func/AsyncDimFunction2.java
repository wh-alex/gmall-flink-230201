package com.atguigu.app.func;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.utils.DimUtil;
import com.atguigu.utils.JedisUtil;
import com.atguigu.utils.MyHBaseDataSource;
import lombok.SneakyThrows;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;
import org.apache.hadoop.hbase.client.Connection;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Collections;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public abstract class AsyncDimFunction2<T> extends RichAsyncFunction<T, T> implements DimJoinFunction<T> {

    private JedisPool jedisPool;
    private MyHBaseDataSource hBaseDataSource;
    private ThreadPoolExecutor threadPoolExecutor;
    private String tableName;

    public AsyncDimFunction2(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        //初始化连接池
        jedisPool = JedisUtil.getJedisPool();
        hBaseDataSource = new MyHBaseDataSource(5);
        threadPoolExecutor = new ThreadPoolExecutor(5, 20, 10, TimeUnit.MINUTES, new LinkedBlockingQueue<>());
    }

    @Override
    public void asyncInvoke(T input, ResultFuture<T> resultFuture) throws Exception {
        threadPoolExecutor.execute(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                //查询维度数据
                Jedis jedis = jedisPool.getResource();
                Connection connection = hBaseDataSource.getConnection();
                JSONObject dimInfo = DimUtil.getDimInfo(jedis, connection, tableName, getRowKey(input));

                //补充维度信息
                join(input, dimInfo);

                //输出
                jedis.close();
                hBaseDataSource.addBack(connection);
                resultFuture.complete(Collections.singletonList(input));
            }
        });
    }

    @Override
    public void timeout(T input, ResultFuture<T> resultFuture) throws Exception {
        System.out.println("TimeOut>>>>>>>>" + input);
    }

    @Override
    public void close() throws Exception {

    }
}
