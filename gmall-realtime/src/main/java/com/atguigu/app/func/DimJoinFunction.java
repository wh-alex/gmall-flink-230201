package com.atguigu.app.func;

import com.alibaba.fastjson.JSONObject;

public interface DimJoinFunction<T> {
    String getRowKey(T value);

    void join(T value, JSONObject dimInfo);
}
