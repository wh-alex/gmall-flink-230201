package com.atguigu.bean;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TrafficHomeDetailPageViewBean {
    // 窗口起始时间
    String stt;
    // 窗口结束时间
    String edt;
    String cur_date;
    // 首页独立访客数
    Long home_uv_ct;
    // 商品详情页独立访客数
    Long good_detail_uv_ct;
    // 时间戳
    @JSONField(serialize = false)
    Long ts;
}
