package com.atguigu.bean;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TrafficPageViewBean {
    // 窗口起始时间
    private String stt;
    // 窗口结束时间
    private String edt;
    // app 版本号
    private String vc;
    // 渠道
    private String ch;
    // 地区
    private String ar;
    // 新老访客状态标记
    private String is_new;
    // 当天日期
    private String cur_date;
    // 独立访客数
    private Long uv_ct;
    // 会话数
    private Long sv_ct;
    // 页面浏览数
    private Long pv_ct;
    // 累计访问时长
    private Long dur_sum;
    // 时间戳
    @JSONField(serialize = false) // 这个字段不需要序列化到json字符串中, 可以加这个注解
    private Long ts;
}
