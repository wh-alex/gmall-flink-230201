package com.atguigu.bean;

import lombok.Data;

/**
 * Desc: 配置表对应实体类
 */
@Data
public class TableProcess {
    // 来源表
    String sourceTable;
    // 来源操作类型
    String sourceType;
    // 输出表
    String sinkTable;
    // 输出类型 dwd | dim
    String sinkType;
    // 输出列族
    String sinkFamily;
    // sink到 hbase 的时候的具体字段
    String sinkColumns;
    // sink到 hbase 的时候的主键字段
    String sinkRowKey;
    // 建表扩展（一些额外参数可以在此配置）
    String sinkExtend;
    //配置表的操作类型
    String op; //crud  为了后续删除状态中的数据
}

